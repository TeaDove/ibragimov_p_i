using System;
public class Programm{
public struct answer{
	public string[] ans;
	public answer(string[] ans){
		this.ans=ans;
	}
}
public struct oneAns{
	public int count;
	public string name;
	public oneAns(string name, int count){
		this.count=count;
		this.name=name;
	}
}
public static void zeroing(oneAns[] arr){
	for(int i=0;i<arr.Length;i++)
		arr[i]=new oneAns("",0);
}
public struct allAns{
	public oneAns[] answer;
	public string question;
	public int fill;
	public allAns(string question, oneAns[] answer)
	{
	this.question = question;
	this.answer = answer;
	fill=0;
	}
}
	public static void insertAns(string a, ref allAns arrAns){
		//Console.WriteLine(a!="-");
		if(a!="-")
		{
			int i;
			for(i=0;i<arrAns.fill;i++)
				if(a==arrAns.answer[i].name){
					break;
				}
			if(i==arrAns.fill){
					arrAns.answer[i].name=a;
					arrAns.fill++;
			}
			arrAns.answer[i].count++;
		}
	}
	public static void coutBestOfFive(allAns arr){
		int sum=0;
		int n=arr.fill;
		for(int i=0;i<n;i++)
			sum+=arr.answer[i].count;
		//Console.WriteLine(sum);
		if(n>5) n=5;
		for(int i=0;i<n;i++)
		{
			int imax=i;
			for(int j=i;j<arr.fill;j++)
				if(arr.answer[imax].count<arr.answer[j].count)
					imax=j;
			oneAns _temp=arr.answer[i];
			arr.answer[i]=arr.answer[imax];
			arr.answer[imax]=_temp;
		}
		/*for(int i=0;i<n;i++)
		{
			int j=i;
			while((j>0)&&(arr.answer[j-1].count<arr.answer[j].count)){
				oneAns _temp=arr.answer[j-1];
				arr.answer[j-1]=arr.answer[j];
				arr.answer[j]=_temp;
				j--;
			}
		}*/
		Console.WriteLine("Question: {0}",arr.question);
		for(int i=0;i<n;i++)
			Console.WriteLine("{0:10}, {1}%",arr.answer[i].name,arr.answer[i].count*100/sum);
		Console.WriteLine("------");
	}
	public static void Main(){
		answer[] arr=new answer[15];
		arr[0]=new answer(new string[]{"-","determination","anime"});
		arr[1]=new answer(new string[]{"-","-","manga"});
		arr[2]=new answer(new string[]{"fox","-","anime"});
		arr[3]=new answer(new string[]{"fox","-","manga"});
		arr[4]=new answer(new string[]{"FOX","determination","anime"});
		arr[5]=new answer(new string[]{"fox","-","manga"});
		arr[6]=new answer(new string[]{"toby","-","anime"});
		arr[7]=new answer(new string[]{"toby","-","manga"});
		arr[8]=new answer(new string[]{"camel","-","anime"});
		arr[9]=new answer(new string[]{"cat","determination","manga"});
		arr[10]=new answer(new string[]{"wolf","-","anime"});
		arr[11]=new answer(new string[]{"Pikachu","-","manga"});
		arr[12]=new answer(new string[]{"Pikachu","-","anime"});
		arr[13]=new answer(new string[]{"man","-","manga"});
		arr[14]=new answer(new string[]{"Pikachu","determination","anime"});
		oneAns[] _temp1=new oneAns[15];
		zeroing(_temp1);
		oneAns[] _temp2=new oneAns[15];
		zeroing(_temp2);
		oneAns[] _temp3=new oneAns[15];
		zeroing(_temp3);
		allAns[] state=new allAns[3]{new allAns("Japanese animal",_temp1),
									 new allAns("Trait",_temp2),
									 new allAns("Thing",_temp3)};
		for(int i=0;i<3;i++)
			for(int j=0;j<15;j++){
				//Console.WriteLine("Sending {0}, {1}",arr[j].ans[i],state[i]);
				insertAns(arr[j].ans[i],ref state[i]);
			}
		//Console.Write(state[0].fill);
		/*for(int i=0;i<3;i++){
			for(int j=0;j<state[i].fill;j++)
				Console.Write("{0}:{1} ",state[i].answer[j].name,state[i].answer[j].count);
			Console.WriteLine();
		}*/
		coutBestOfFive(state[0]);
		coutBestOfFive(state[1]);
		coutBestOfFive(state[2]);
		}
}
