#include <iostream>
#include <sstream>
#include <complex/complex.h>

Complex::Complex(const double real):Complex(real,0.0){}

Complex::Complex(const double real, const double imaginary):re(real),im(imaginary){}


std::istream& Complex::readFrom(std::istream &istrm) {
	char left_brace(0);
	double real(0.0);
	char comma(0);
	double imaginary(0.0);
	char right_brace(0);
	istrm >> left_brace >> real >> comma >> imaginary >> right_brace;
	if (istrm.good()) {
		if ((Complex::left_brace == left_brace) && (Complex::separator == comma) && (Complex::right_brace == right_brace)) {
			re = real;
		 	im = imaginary;
		}
	       	else {
			istrm.setstate(std::ios_base::failbit);
		}
	}
	return istrm;
}

std::ostream& Complex::writeTo(std::ostream& ostrm)const{
	ostrm<<left_brace<< re << separator << im << right_brace;
	return ostrm;
}

bool Complex :: operator==(const Complex &rhs) const{
	return DoubleEquals(re, rhs.re) && DoubleEquals(im, rhs.im);
}

bool Complex :: operator!=(const Complex &rhs) const{
	return !operator==(rhs);
}

bool Complex :: DoubleEquals(const double a, const double b, const double epsilon) {
	return std::abs(a - b) < epsilon;
}

Complex &Complex::operator+=(const Complex &rhs){
	re+=rhs.re;
	im+=rhs.im;
	return *this;
}

Complex &Complex::operator+=(const double rhs){
	re+=rhs;
	return *this;
}

Complex &Complex::operator-=(const Complex &rhs){
	re-=rhs.re;
	im-=rhs.im;
	return *this;
}

Complex &Complex::operator-=(const double rhs){
	re-=rhs;
	return *this;
}

Complex &Complex::operator*=(const Complex &rhs){
	double a=re*rhs.re;
	double b=re*rhs.im;
	double c=im*rhs.re;
	double d=im*rhs.im;
	re=a+d;
	im=b+c;
	return *this;
}

Complex &Complex::operator*=(const double rhs){
	re*=rhs;
	im*=rhs;
	return *this;
}

Complex &Complex::operator/=(const Complex &rhs){
	double tempRe=(re*rhs.re+im*rhs.im)/(rhs.re*rhs.re+rhs.im*rhs.im);
	double tempIm=(re*rhs.re-im*rhs.im)/(rhs.re*rhs.re+rhs.im*rhs.im);
	re=tempRe; //Я блин не иронично этот язык ненавижу, зачем все блин обьявлять почему нельзя как в питоне просто а.
	im=tempIm;
	return *this;
}
Complex &Complex::operator/=(const double rhs){
	re/=rhs;
	im/=rhs;
	return *this;
}

Complex operator+(const Complex& lhs, const Complex& rhs){
	return Complex(lhs.re+rhs.re,lhs.im+rhs.im);
}

Complex operator+(const Complex& lhs, const double rhs){
	return Complex(lhs.re+rhs,lhs.im);
}

Complex operator+(const double lhs, const Complex& rhs){
	return Complex(rhs.re+lhs,rhs.im);
}

Complex operator-(const Complex& lhs, const Complex& rhs){
	return Complex(lhs.re-rhs.re,lhs.im-rhs.im);
}

Complex operator-(const Complex& lhs, const double rhs){
	return Complex(lhs.re-rhs,lhs.im);
}

Complex operator-(const double lhs, const Complex& rhs){
	return Complex(-rhs.re+lhs,-rhs.im);
}

Complex operator*(const Complex& lhs, const Complex& rhs){
	Complex tmp(lhs);
	tmp*=rhs;
	return tmp;
}

Complex operator*(const Complex& lhs, const double rhs){
	return Complex(lhs.re*rhs,lhs.im*rhs);
}

Complex operator*(const double lhs, const Complex& rhs){
	return Complex(rhs.re*lhs,rhs.im*lhs);
}

Complex operator/(const Complex& lhs, const Complex& rhs){
	Complex tmp(lhs);
	tmp/=rhs;
	return tmp;
}

Complex operator/(const Complex& lhs, const double rhs){
	return Complex(lhs.re/rhs,lhs.im/rhs);
}

Complex operator/(const double lhs, const Complex& rhs){
	return Complex(rhs.re/lhs,rhs.im/lhs);
}
