#include <iostream>
#include <sstream>
#include <vector>
#include <long_int.long_int.h>
#include <string>

class Lint{
	public:
		Lint();
		Lint(const string& number);
		~Lint();

		bool operator==(const Lint& rhs);
		bool operator!=(const Lint& rhs);


	private:
		std::vector<int32_t> num_{};
		bool is_neg_;
}
