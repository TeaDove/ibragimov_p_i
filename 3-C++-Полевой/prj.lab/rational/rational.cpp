#include <iostream>
#include <sstream>
#include <rational/rational.h>

Rational::Rational(const int numerator, const int denominator):num_(numerator){
	if (denominator == 0)
		throw std::invalid_argument("Denominator cannot be zero");
	den_=denominator;
	Simplify();
}
Rational::Rational(const int numerator):num_(numerator),den_(1) {}

std::istream& Rational::readFrom(std::istream &istrm) {
	char left_brace(0);
	double numerator(0);
	char separator(0);
	double denominator(1);
	char right_brace(0);
	istrm >> left_brace >> numerator >> separator >> denominator >> right_brace;
	if (istrm.good()) {
		if ((Rational::left_brace_ == left_brace) && (Rational::separator_ == separator) && (Rational::right_brace_) == right_brace) {
			if (denominator == 0)
				throw std::invalid_argument("Denominator cannot be zero");
			num_ = numerator;
		 	den_ = denominator;
			Simplify();
		}
	       	else {
			istrm.setstate(std::ios_base::failbit);
		}
	}
	return istrm;
}

std::ostream& Rational::writeTo(std::ostream& ostrm)const{
	ostrm<<left_brace_<< num_ << separator_ << den_ << right_brace_;
	return ostrm;
}

void Rational::Simplify(){
	if (den_<0){
		num_*=-1;
		den_*=-1;
	}
	int a=num_;
	int b=den_;
	while (b){
		a %= b;
		std::swap(a, b);
	}
	num_/=a;
	den_/=a;
}

bool Rational::operator==(const Rational &rhs) const{
	return ((num_==rhs.num_)&&(den_==rhs.den_));
}

bool Rational::operator!=(const Rational &rhs) const{
	return !operator==(rhs);
}

bool Rational::operator==(const int rhs) const{
	return (num_==rhs*den_);
}

bool Rational::operator!=(const int rhs) const{
	return !operator==(rhs);
}


Rational &Rational::operator+=(const Rational &rhs){
	num_=num_*rhs.den_+rhs.num_*den_;
    	den_*=rhs.den_;
	Simplify();
	return *this;
}

Rational &Rational::operator+=(const int rhs){
	num_+=rhs*den_;
	Simplify();
	return *this;
}

Rational &Rational::operator-=(const Rational &rhs){
	num_=num_*rhs.den_-rhs.num_*den_;
    	den_*=rhs.den_;
	Simplify();
	return *this;
}

Rational &Rational::operator-=(const int rhs){
	num_-=rhs*den_;
	Simplify();
	return *this;
}

Rational &Rational::operator*=(const Rational &rhs){
	num_*=rhs.num_;
	den_*=rhs.den_;
	Simplify();
	return *this;
}

Rational &Rational::operator*=(const int rhs){
	num_*=rhs;
	Simplify();
	return *this;
}

Rational &Rational::operator/=(const Rational &rhs){
	num_*=rhs.den_;
	den_*=rhs.num_;
	Simplify();
	return *this;
}
Rational &Rational::operator/=(const int rhs){
	den_*=rhs;
	Simplify();
	return *this;
}

Rational operator+(const Rational& lhs, const Rational& rhs){
	Rational tmp(lhs);
	tmp+=rhs;
	return tmp;
}

Rational operator+(const Rational& lhs, const int rhs){
	Rational tmp(lhs);
	tmp+=rhs;
	return tmp;
}

Rational operator+(const int lhs, const Rational& rhs){
	Rational tmp(rhs);
	tmp+=lhs;
	return tmp;
}

Rational operator-(const Rational& lhs, const Rational& rhs){
	Rational tmp(lhs);
	tmp-=rhs;
	return tmp;
}

Rational operator-(const Rational& lhs, const int rhs){
	Rational tmp(lhs);
	tmp-=rhs;
	return tmp;
}

Rational operator-(const int lhs, const Rational& rhs){
	Rational tmp(rhs);
	tmp-=lhs;
	return tmp;
}

Rational operator*(const Rational& lhs, const Rational& rhs){
	Rational tmp(lhs);
	tmp*=rhs;
	return tmp;
}

Rational operator*(const Rational& lhs, const int rhs){
	Rational tmp(lhs);
	tmp*=rhs;
	return tmp;
}

Rational operator*(const int lhs, const Rational& rhs){
	Rational tmp(rhs);
	tmp*=lhs;
	return tmp;
}

Rational operator/(const Rational& lhs, const Rational& rhs){
	Rational tmp(lhs);
	tmp/=rhs;
	return tmp;
}

Rational operator/(const Rational& lhs, const int rhs){
	Rational tmp(lhs);
	tmp/=rhs;
	return tmp;
}

Rational operator/(const int lhs, const Rational& rhs){
	Rational tmp(rhs);
	tmp/=lhs;
	return tmp;
}
