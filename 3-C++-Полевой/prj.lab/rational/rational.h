
#ifndef RATIONAL_RATIONAL_H_20191010
#define RATIONAL_RATIONAL_H_20191010


#include <iostream>
#include <sstream>

struct Rational{
	//numerator(num) - числитель denominator(den) - знаменатель
	Rational(const Rational&) = default;
	Rational(const int numerator, const int denominator);
	Rational(const int numerator);
	~Rational() = default;
	Rational& operator=(const Rational&) = default;
	Rational(Rational&&) = default;
	Rational& operator=(Rational&&) = default;

	bool operator==(const Rational& rhs) const;
	bool operator!=(const Rational& rhs) const;
	bool operator==(const int rhs) const;
	bool operator!=(const int rhs) const;

	void Simplify();

	Rational& operator+=(const Rational& rhs);
	Rational& operator+=(const int rhs);

	Rational& operator-=(const Rational& rhs);
	Rational& operator-=(const int rhs);

	Rational& operator*=(const Rational& rhs);
	Rational& operator*=(const int rhs);

	Rational& operator/=(const Rational& rhs);
	Rational& operator/=(const int rhs);

	std::ostream& writeTo(std::ostream& ostrm) const;
	std::istream& readFrom(std::istream& istrm);

	double num_{0};
	double den_{1};

	static const char left_brace_{'{'};
	static const char separator_{'/'};
	static const char right_brace_{'}'};
};

Rational operator+(const Rational& lhs, const Rational& rhs);
Rational operator+(const Rational& lhs, const int rhs);
Rational operator+(const int lhs, const Rational& rhs);

Rational operator-(const Rational& lhs, const Rational& rhs);
Rational operator-(const Rational& lhs, const int rhs);
Rational operator-(const int lhs, const Rational& rhs);

Rational operator*(const Rational& lhs, const Rational& rhs);
Rational operator*(const Rational& lhs, const int rhs);
Rational operator*(const int lhs, const Rational& rhs);

Rational operator/(const Rational& lhs, const Rational& rhs);
Rational operator/(const Rational& lhs, const int rhs);
Rational operator/(const int lhs, const Rational& rhs);

inline std::ostream& operator<<(std::ostream& ostrm, const Rational& rhs) {
	  return rhs.writeTo(ostrm);
}

inline std::istream& operator>>(std::istream& istrm, Rational& rhs) {
	  return rhs.readFrom(istrm);
}




#endif
