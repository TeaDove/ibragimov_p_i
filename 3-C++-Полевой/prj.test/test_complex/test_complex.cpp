#include <iostream>
#include <sstream>
#include "complex/complex.h"


int main(){
    using namespace std;
    cout<<"Test of complex structur"<<endl;
    cout<<"Initialize instance a={5.6,7.2} and b={4.4,2.8}"<<endl;
    Complex a(5.6,7.2);
    Complex b(a);
    b-=Complex(1.2,4.4);
    cout<<a<<' '<<b<<endl;
    cout<<"a+=b ";
    a+=b;
    cout<<a<<endl<<"a-=b ";
    a-=b;
    cout<<a<<endl<<"a*=b ";
    a*=b;
    cout<<a<<endl<<"a/=b ";
    a/=b;
    cout<<a<<endl<<"And make a be {5.6,7.2} again,";
    a=Complex(5.6,7.2);
    cout<<" a= "<<a<<endl<<"a+b "<<a+b<<endl<<"a-b "<<a-b<<endl<<"a*b "<<a*b<<endl<<"a/b "<<a/b<<endl;
    cout<<"a+5 "<<a+5<<endl<<"a-5 "<<a-5<<endl<<"a*5 "<<a*5<<endl<<"a/5 "<<a/5<<endl;
    cout<<"5+b "<<5+b<<endl<<"5-b "<<5-b<<endl<<"5*b "<<5*b<<endl<<"5/b "<<5/b<<endl;
    cout<<"Compare a with {5,7}: "<<(a==Complex(5,7))<<endl;
    cout<<"Compare a with {5.6,7.2}: "<<(a==Complex(5.6,7.2))<<endl;
    cout<<"antiCompare a with {5,7}: "<<(a!=Complex(5,7))<<endl;
    cout<<"antiCompare a with {5.6,7.2}: "<<(a!=Complex(5.6,7.2))<<endl;

    return 0;
}
