#include <iostream>
#include <sstream>
#include "rational/rational.h"


int main(){
    using namespace std;
    cout<<"Test of rational structur"<<endl;
    cout<<"Initialize instance a={420/69} and b={60,50}"<<endl;
    Rational a(420,69);
    Rational b(60,50);
    cout<<a<<' '<<b<<endl;
    cout<<"a+=b ";
    a+=b;
    cout<<a<<endl<<"a-=b ";
    a-=b;
    cout<<a<<endl<<"a*=b ";
    a*=b;
    cout<<a<<endl<<"a/=b ";
    a/=b;
    cout<<a<<endl<<"And make a {60,50},";
    a=Rational(60,50);
    cout<<" a= "<<a<<endl<<"a+b "<<a+b<<endl<<"a-b "<<a-b<<endl<<"a*b "<<a*b<<endl<<"a/b "<<a/b<<endl;
    cout<<"a+5 "<<a+5<<endl<<"a-5 "<<a-5<<endl<<"a*5 "<<a*5<<endl<<"a/5 "<<a/5<<endl;
    cout<<"5+b "<<5+b<<endl<<"5-b "<<5-b<<endl<<"5*b "<<5*b<<endl<<"5/b "<<5/b<<endl;
    cout<<"Compare a with {10/2}: "<<(a==Rational(10,2))<<endl;
    cout<<"Compare a with {12/10}: "<<(a==Rational(12,10))<<endl;
    cout<<"antiCompare a with {10/2}: "<<(a!=Rational(10,2))<<endl;
    cout<<"antiCompare a with {12/10}: "<<(a!=Rational(12,10))<<endl;
    cout<<"Compare {10/2} and 5: "<<(Rational(10,2)==5)<<endl;
    cout<<"Compare {10/2} and 4: "<<(Rational(10,2)==4)<<endl;
    cout<<"antiCompare {10/2} and 5: "<<(Rational(10,2)!=5)<<endl;
    cout<<"antiCompare {10/2} and 4: "<<(Rational(10,2)!=4)<<endl;
    return 0;
}
