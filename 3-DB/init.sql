-- Пересоздает схему public таким образом, что все связанные таблицы, функции и тд удаляются
-- Пересоздает схему public таким образом, что все связанные таблицы, функции и тд удаляются
drop schema public cascade;
create schema public;

-- Создание таблиц и связей

CREATE TABLE "user"
(
    "id"          serial PRIMARY KEY,
    "employee_id" integer,
    "username"    text UNIQUE,
    "password"    text,
    "role"        text,
    "user_email"  text UNIQUE,
    "created_at"  timestamp with time zone default now()
);

create table "user_log"
(
    "id"              serial PRIMARY KEY,
    "user_id"         integer,
    "created_at"      timestamp with time zone default now(),

    "new_employee_id" integer,
    "new_username"    text,
    "new_password"    text,
    "new_role"        text,
    "new_user_email"  text
);

CREATE TABLE "employee"
(
    "id"            serial PRIMARY KEY,
    "department_id" integer,
    "last_name"     text,
    "first_name"    text,
    "father_name"   text,
    "phone_number"  text,
    "contact_email" text,
    "passport_id"   text,
    "created_at"    timestamp with time zone default now()
);

CREATE TABLE "department"
(
    "id"    serial PRIMARY KEY,
    "title" text
);

CREATE TABLE "shift_log"
(
    "id"                     serial PRIMARY KEY,
    "employee_id"            integer,
    "shift_date"             date,
    "hours_per_day"          integer,
    "overtime_hours_per_day" integer
);

CREATE TABLE "employee_job_info"
(
    "id"              serial PRIMARY KEY,
    "employee_id"     integer,
    "remote_workflow" boolean,
    "work_schedule"   text,
    "job_title"       TEXT,
    "salary"          integer
);

CREATE TABLE "project"
(
    "id"                 serial PRIMARY KEY,
    "contract_id"        integer,
    "planned_start_date" date,
    "planned_end_date"   date,
    "real_start_date"    date,
    "object_count"       integer,
    "budget"             integer,
    "description"        text
);

CREATE TABLE "contract"
(
    "id"               serial PRIMARY KEY,
    "client_id"        integer,
    "contract_law_id"  text,
    "signed_at"        date,
    "price"            integer,
    "contract_content" text
);

CREATE TABLE "client"
(
    "id"                serial PRIMARY KEY,
    "client_short_name" text,
    "client_type"       text,
    "created_at"        date default now(),
    "job_title"         text,
    "phone_number"      text,
    "contact_email"     text
);

CREATE TABLE "physical_entity"
(
    "id"              serial PRIMARY KEY,
    "client_id"       integer,
    "last_name"       text,
    "first_name"      text,
    "father_name"     text,
    "passport_id"     text,
    "expense_account" text,
    "BIK"             text,
    "bank"            text
);

CREATE TABLE "legal_entity"
(
    "id"                serial PRIMARY KEY,
    "client_id"         integer,
    "OGRN"              text,
    "INN"               text,
    "KPP"               text,
    "registered_at"     date,
    "owner_last_name"   text,
    "owner_first_name"  text,
    "owner_father_name" text
);

CREATE TABLE "object"
(
    "id"                 serial PRIMARY KEY,
    "project_id"         integer,
    "area_id"            integer,
    "object_title"       text,
    "floor_count"        integer,
    "room_count"         integer,
    "capacity"           integer,
    "electricity"        text,
    "heat_communication" text
);

CREATE TABLE "area"
(
    "id"              serial PRIMARY KEY,
    "city_id"         integer,
    "area_type"       text,
    "index"           text,
    "full_address"    text,
    "owner"           text,
    "registration_id" text,
    "latitude"        text,
    "longitude"       text,
    "area_size"       integer
);

CREATE TABLE "city"
(
    "id"           serial PRIMARY KEY,
    "city_name"    text,
    "city_type"    text,
    "region"       integer,
    "latitude"     text,
    "longitude"    text,
    "population"   integer,
    "climate_zone" text
);

CREATE TABLE "drawing"
(
    "id"         serial PRIMARY KEY,
    "project_id" integer,
    "zip_file"   bytea
);

CREATE TABLE "drawings_author"
(
    "id"          serial PRIMARY KEY,
    "drawing_id"  integer,
    "employee_id" integer
);

CREATE TABLE "employee_assigned_for_project"
(
    "id"          serial PRIMARY KEY,
    "employee_id" integer,
    "project_id"  integer,
    "assigned_at" date,
    "finish_at"   date
);

alter table "employee_assigned_for_project"
    add unique ("employee_id", "project_id");

ALTER TABLE "user"
    ADD FOREIGN KEY ("employee_id") REFERENCES "employee" ("id");

ALTER TABLE "employee"
    ADD FOREIGN KEY ("department_id") REFERENCES "department" ("id");

alter table "shift_log"
    add foreign key ("employee_id") references "employee" ("id");

ALTER TABLE "employee_job_info"
    ADD FOREIGN KEY ("employee_id") REFERENCES "employee" ("id");

ALTER TABLE "project"
    ADD FOREIGN KEY ("contract_id") REFERENCES "contract" ("id");

ALTER TABLE "contract"
    ADD FOREIGN KEY ("client_id") REFERENCES "client" ("id");

ALTER TABLE "physical_entity"
    ADD FOREIGN KEY ("client_id") REFERENCES "client" ("id");

ALTER TABLE "legal_entity"
    ADD FOREIGN KEY ("client_id") REFERENCES "client" ("id");

ALTER TABLE "object"
    ADD FOREIGN KEY ("project_id") REFERENCES "project" ("id");

ALTER TABLE "area"
    ADD FOREIGN KEY ("city_id") REFERENCES "city" ("id");

ALTER TABLE "drawing"
    ADD FOREIGN KEY ("project_id") REFERENCES "project" ("id");

-- Триггеры

-- Логирование изменений в "user"
create or replace function create_user_log_proc() returns trigger
    language plpgsql
as
$$
BEGIN
    insert into user_log (user_id, new_employee_id, new_username, new_password, new_role, new_user_email)
    VALUES (NEW.id, NEW.employee_id, NEW.username, NEW.password, NEW.role, NEW.user_email);

    return NEW;
END;
$$;

create or replace trigger create_user_log
    after insert or update
    on "user"
    for each row
execute procedure create_user_log_proc();

insert into "user" (username, password, role, user_email)
values (gen_random_uuid()::text, gen_random_uuid()::text, 'charger', gen_random_uuid()::text);

-- Логирование потенциального мощеничествого аккаунта

create or replace function antifraud_check_proc() returns trigger
    language plpgsql
as
$$
BEGIN
    if NEW.user_email ~ '[0-9]+' then
        raise warning 'email contains digits, possible fraud %', NEW.user_email;
    end if;

    if length(NEW.username) > 12 then
        raise warning 'username too long, possible fraud %', NEW.username;
    end if;

    return NEW;
END;
$$;

create or replace trigger antifraud_check
    after insert or update
    on "user"
    for each row
execute procedure antifraud_check_proc();

insert into "user" (username, password, role, user_email)
values (gen_random_uuid(), '123', 'charger', gen_random_uuid());

insert into "user" (username, password, role, user_email)
values ('TeaDove', '123', 'charger', 'pibragimov@whoosh.bike');


-- Удаления персональных данных пользователя из user_log после удаления юзера

create or replace function prune_personal_data_proc() returns trigger
    language plpgsql
as
$$
BEGIN
    update user_log set new_user_email = '******', new_username = '******' where user_id = OLD.id;

    return new;
END;
$$;

create or replace trigger prune_personal_data
    after delete
    on "user"
    for each row
execute procedure prune_personal_data_proc();

insert into "user" (username, password, role, user_email)
values ('maria', '123', 'charger', gen_random_uuid());
update "user"
set user_email = 'maria@gmail.com'
where username = 'maria';
delete
from "user"
where username = 'maria';

select *
from user_log
order by id desc;

-- Процедуры

-- Захешировать пароль, если у пользователя еще нет employee_id или его employee.phone_number не null

create or replace procedure hash_password()
    language plpgsql
as
$$
BEGIN
    update "user" u
    set password = md5(password)
    where u.id in (select u.id
                   from "user" u
                            left join employee e on u.employee_id = e.id
                   where e.phone_number is not null
                      or e.id is null);
END;
$$;

call hash_password();
select *
from "user";


-- Вычислить площадь всех зон по городу

create or replace procedure calculate_area(area_city_name text, radius float)
    language plpgsql
as
$$
BEGIN
    update area a
    set area_size = radius * radius * 3.141592653589793238462643383279502884197169399
    where a.id in (select a.id
                   from "area" a
                            join city c on a.city_id = c.id
                   where c.city_name = area_city_name
                     and city_type = 'Столица');
END;
$$;

call calculate_area('Moscow', 15);


-- Повысить ЗП всем работникам, кто работает больше 6х месяцев, но меньше 12 месяцев

create or replace procedure promote()
    language plpgsql
as
$$
BEGIN
    update employee_job_info eji
    set salary = eji.salary * 1.2
    where eji.id in (select eji2.id
                     from employee e
                              join employee_job_info eji2 on eji2.employee_id = e.id
                              join department d on e.department_id = d.id
                     where now() - interval '6 months' > e.created_at
                       and now() - interval '12 months' < e.created_at
                       and d.title = 'Architecture');
END;
$$;

call promote();


-- Функции

create or replace function get_employee_salary()
    returns table
            (
                employee_id    integer,
                full_name      text,
                monthly_salary numeric
            )
    language plpgsql
as
$$
begin
    return query
        select e.id,
               concat(e.first_name, ' ', e.last_name)                              as full_name,
               ej.salary * sum(sl.hours_per_day + sl.overtime_hours_per_day * 1.5) as monthly_salary
        from employee e
                 join shift_log sl on e.id = sl.employee_id
                 join employee_job_info ej on e.id = ej.employee_id
        where date_trunc('month', sl.shift_date) = date_trunc('month', current_date)
        group by e.id, full_name, ej.salary;
end;
$$;

create or replace function get_projects_and_drawings_by_employee_id(emp_id integer)
    returns table
            (
                project_description text,
                drawing             bytea,
                employee_full_name text
            )
    language plpgsql
as
$$
begin
    return query select p.description, d.zip_file, e.first_name || ' ' || e.last_name from employee e
                          join employee_assigned_for_project ea on ea.employee_id = e.id
                          join project p on ea.project_id = p.id
                          join drawing d on p.id = d.project_id
                          where e.id = emp_id;
end;
$$;

create or replace function get_architecture_employees()
    returns setof "employee"
    language plpgsql as
$$
begin
    return query select *
                 from "employee"
                 where "department_id" = (select "id"
                                          from "department"
                                          where "title" = 'Architecture');

    return;
end;
$$;

-- Роли

-- Юзеры

-- Вставка данных

INSERT INTO "department" ("title")
VALUES ('Human Resources'),
       ('Finance'),
       ('Architecture');



INSERT INTO "employee" ("department_id", "last_name", "first_name", "father_name", "phone_number", "contact_email",
                        "passport_id", "created_at")
VALUES (1, 'Иванов', 'Алексей', 'Сергеевич', '+79123456789', 'alekseyivanov@example.com', '1234 567890',
        '2023-12-28 23:10:00'),
       (2, 'Смирнова', 'Екатерина', 'Андреевна', '+79987654321', 'ekaterinasmirnova@example.com', '9876 543210',
        '2023-12-28 23:10:00'),
       (3, 'Петров', 'Михаил', 'Иванович', '+79111222333', 'mihailpetrov@example.com', '6543 210987',
        '2022-12-28 23:10:00'),
       (3, 'Сидорова', 'Ольга', 'Дмитриевна', '+79444555666', 'olgasidorova@example.com', '3210 987654',
        '2023-05-28 23:10:00'),
       (3, 'Козлов', 'Денис', 'Александрович', '+79777888999', 'deniskozlov@example.com', '9876 543210',
        '2023-12-28 23:10:00');

INSERT INTO "employee_job_info" ("employee_id", "remote_workflow", "work_schedule", "job_title", "salary")
VALUES (3, true, 'Полный рабочий день', 'HR', 10000),
       (1, FALSE, 'Гибкий график', 'Бухгалтер', 12000),
       (5, TRUE, 'Удаленная работа', 'Главный архитектор', 56000),
       (4, FALSE, 'Полный рабочий день', 'Архитектор', 23000),
       (2, TRUE, 'Гибкий график', 'Архитектор', 35000);

INSERT INTO "shift_log" ("employee_id", "shift_date", "hours_per_day", "overtime_hours_per_day")
VALUES (1, '2023-12-28', 8, 2),
       (2, '2023-12-28', 7, 1),
       (3, '2023-12-28', 8, 3),
       (4, '2023-12-28', 7, 0),
       (5, '2023-12-28', 8, 1);



INSERT INTO "client" ("client_short_name", "client_type", "created_at", "job_title", "phone_number", "contact_email")
VALUES ('ООО Архитектурное бюро', 'Юридическое лицо', '2023-12-29', 'Директор', '+79123456789', 'director@example.com'),
       ('Иванов Иван Иванович', 'Физическое лицо', '2023-12-29', null, '+79987654321', 'ivanov@example.com'),
       ('ООО Строительная компания', 'Юридическое лицо', '2023-12-29', 'Генеральный директор', '+79111222333',
        'ceo@example.com'),
       ('Петрова Мария Сергеевна', 'Физическое лицо', '2023-12-29', null, '+79444555666', 'petrova@example.com'),
       ('ООО Дизайн-студия', 'Юридическое лицо', '2023-12-29', 'Руководитель проектов', '+79777888999',
        'manager@example.com'),
       ('ООО Архитектурная студия', 'Юридическое лицо', '2023-12-29', 'Архитектор', '+79001112233',
        'archstudio@example.com'),
       ('Сидорова Елена Петровна', 'Физическое лицо', '2023-12-29', null, '+79123456789', 'elena.sidorova@example.com'),
       ('ООО Строительный Холдинг', 'Юридическое лицо', '2023-12-29', 'Главный инженер', '+79998887766',
        'holding@example.com'),
       ('Иванова Анна Сергеевна', 'Физическое лицо', '2023-12-29', null, '+79223344556', 'anna.ivanova@example.com'),
       ('ООО Архитектурное агентство', 'Юридическое лицо', '2023-12-29', 'Менеджер проектов', '+79776655443',
        'agency@example.com');

INSERT INTO "physical_entity" ("client_id", "last_name", "first_name", "father_name", "passport_id", "expense_account",
                               "BIK", "bank")
VALUES (2, 'Иванов', 'Иван', 'Иванович', '1234 567890', '12345678901234567890', '044525225', 'Альфа-Банк'),
       (4, 'Петрова', 'Мария', 'Сергеевна', '9876 543210', '09876543210987654321', '042202103', 'Сбербанк'),
       (6, 'Смирнов', 'Алексей', 'Петрович', '5678 901234', '98765432109876543210', '044525225', 'Альфа-Банк'),
       (8, 'Андреева', 'Елена', 'Сергеевна', '3456 789012', '12345678901234567890', '042202103', 'Сбербанк'),
       (10, 'Кузнецов', 'Дмитрий', 'Игоревич', '7890 123456', '09876543210987654321', '044525225', 'Альфа-Банк');

INSERT INTO "legal_entity" ("client_id", "OGRN", "INN", "KPP", "registered_at", "owner_last_name", "owner_first_name",
                            "owner_father_name")
VALUES (1, '1234567890123', '1234567890', '123456789', '2023-01-01', 'Иванов', 'Иван', 'Петрович'),
       (3, '9876543210987', '0987654321', '987654321', '2022-05-15', 'Петров', 'Петр', 'Иванович'),
       (5, '3456789012345', '5678901234', '567890123', '2023-06-30', 'Смирнов', 'Алексей', 'Петрович'),
       (7, '7890123456789', '9012345678', '901234567', '2022-12-15', 'Андреева', 'Елена', 'Сергеевна'),
       (9, '2345678901234', '4567890123', '456789012', '2023-09-20', 'Кузнецов', 'Дмитрий', 'Игоревич');



INSERT INTO "contract" ("client_id", "contract_law_id", "signed_at", "price", "contract_content")
VALUES (1, 'ABCD1234', '2023-12-31', 100000, 'Жилой комплекс "Кандинский"'),
       (2, 'EFGH5678', '2024-02-01', 50000, 'Частный коттедж 345'),
       (3, 'IJKL9101', '2024-03-16', 80000, 'Омское метро'),
       (4, 'MNOP1213', '2024-05-01', 30000, 'Школа 145'),
       (5, 'QRST1415', '2024-07-01', 60000, 'ЖК "Бирюзовый"');

INSERT INTO "project" ("contract_id", "planned_start_date", "planned_end_date", "real_start_date", "object_count",
                       "budget", "description")
VALUES (1, '2023-12-31', '2024-01-31', NULL, 5, 100000, 'Многоэтажный дом типа А2'),
       (2, '2024-02-01', '2024-03-15', null, 5, 50000, 'Частный коттедж 145'),
       (3, '2024-03-16', '2024-04-30', NULL, 4, 80000, 'Станция'),
       (4, '2024-05-01', '2024-06-30', null, 3, 30000, 'Школа'),
       (5, '2024-07-01', '2024-08-15', null, 6, 60000, 'Многоэтажный дом типа А2'),
       (5, '2024-07-01', '2024-08-15', NULL, 3, 60000, 'Многоэтажный дом типа С2'),
       (5, '2024-07-01', '2024-08-15', NULL, 6, 60000, 'Проект дворового пространства'),
       (5, '2024-07-01', '2024-08-15', NULL, 6, 60000, 'Подземная парковка');

INSERT INTO "object" ("project_id", "area_id", "object_title", "floor_count", "room_count", "capacity", "electricity",
                      "heat_communication")
VALUES (1, 1, 'жилой дом 1', 5, 10, 100, 'Есть', 'Центральное отопление'),
       (1, 1, 'жилой дом 1', 5, 10, 100, 'Есть', 'Центральное отопление'),
       (1, 1, 'жилой дом 1', 5, 10, 100, 'Есть', 'Центральное отопление'),
       (1, 1, 'жилой дом 1', 5, 10, 100, 'Есть', 'Центральное отопление'),
       (1, 1, 'жилой дом 1', 5, 10, 100, 'Есть', 'Центральное отопление'),
       (2, 2, 'Офисное здание 2', 7, 15, 150, 'Есть', 'Центральное отопление'),
       (3, 3, 'Жилой дом 1', 3, 6, 50, 'Есть', 'Индивидуальное отопление'),
       (4, 4, 'Жилой дом 2', 4, 8, 80, 'Есть', 'Индивидуальное отопление'),
       (5, 5, 'Торговый центр 1', 2, 20, 200, 'Есть', 'Центральное отопление');



INSERT INTO "city" ("city_name", "city_type", "region", "latitude", "longitude", "population", "climate_zone")
VALUES ('Москва', 'Столица', 77, '55.751244', '37.618423', 12655050, 'Умеренный'),
       ('Санкт-Петербург', 'Город федерального значения', 78, '59.938630', '30.314130', 5398064, 'Умеренный'),
       ('Екатеринбург', 'Город федерального значения', 66, '56.838011', '60.597465', 1483119, 'Умеренный'),
       ('Новосибирск', 'Город федерального значения', 54, '55.030199', '82.920430', 1625631, 'Умеренный'),
       ('Красноярск', 'Город федерального значения', 24, '56.015283', '92.893247', 1095281, 'Умеренный');

INSERT INTO "area" ("city_id", "area_type", "index", "full_address", "owner", "registration_id", "latitude",
                    "longitude", "area_size")
VALUES (1, 'Жилой', '12345', 'ул. Центральная, д. 1', 'Иванов', 'REG-123', '55.12345', '37.54321', 100),
       (1, 'Жилой', '54321', 'ул. Офисная, д. 10', 'Петров', 'REG-456', '55.54321', '37.12345', 500),
       (5, 'Общественное', '67890', 'ул. Торговая, д. 5', 'Сидоров', 'REG-789', '55.98765', '37.56789', 200),
       (5, 'Общественное', '67820', 'ул. Промышленная, д. 15', 'Козлов', 'REG-012', '55.56789', '37.98765', 1000),
       (5, 'Жилой', '54321', 'ул. Парковая, д. 20', 'Иванов', 'REG-345', '55.23456', '37.65432', 300);


INSERT INTO "drawing" ("project_id", "zip_file")
VALUES (1, E'\\x54686973206973206120746573742066696c652e'),
       (2, E'\\x5468697320697320616e6f7468657220746573742066696c65'),
       (3, E'\\x5468697320697320616e6f7468657220746573742066696c652e'),
       (4, E'\\x5468697320697320616e6f7468657220746573742066696c652e'),
       (5, E'\\x5468697320697320616e6f7468657220746573742066696c652e');

-- Таблица "drawings_author"
INSERT INTO "drawings_author" ("drawing_id", "employee_id")
VALUES (1, 4),
       (2, 4),
       (3, 3),
       (3, 4),
       (3, 5),
       (5, 5),
       (4, 4);

-- Таблица "employee_assigned_for_project"
INSERT INTO "employee_assigned_for_project" ("employee_id", "project_id", "assigned_at", "finish_at")
VALUES (3, 1, '2023-01-01', '2023-02-28'),
       (3, 2, '2023-03-01', '2023-04-30'),
       (3, 3, '2023-05-01', '2023-06-30'),
       (4, 1, '2023-07-01', '2023-08-31'),
       (4, 2, '2023-09-01', '2023-10-31'),
       (5, 3, '2023-11-01', '2023-12-31'),
       (5, 1, '2024-01-01', '2024-02-29');
