#include <vector>
#include <iostream>

using namespace std;

void dfs (int v,vector<vector<int>>& g,vector<int>& color,vector<int>& path) {
    color[v] = 1;
    path.push_back(v);
    //cout << "Working with i: " << (v+1) << endl;
    for (vector<int>::iterator i=g[v].begin(); i!=g[v].end(); ++i) {
        if (color[*i] == 0) {
            dfs(*i, g, color, path);
            if ((path[path.size()-1]!=v)||(path.size()==0))
                path.push_back(v);
        }
    }
    color[v] = 2;
}
int main(){
    vector < vector<int> > g;
    vector<int> color;
    vector<int> path;
    int N, M, v;
    cin>>N;
    cin>>M;
    cin>>v;
    v--;
    int n=0;
    vector<int> a{};
    for(int i=0;i<N;i++) {
        g.push_back(a);
        color.push_back(0);
    }
    for(int i=0;i<M;i++){
        int X,Y;
        cin>>X;
        cin>>Y;
        g[X-1].push_back(Y-1);
        g[Y-1].push_back(X-1);
    }
    dfs(v,g,color,path);
//    for (int i=0;i<g[g.size()-1].size();i++)
//        if ((g[g.size()][i]==v)&&(v==1))
//            path.push_back()
    cout<<path.size()<<endl;
    for(int i=0;i<path.size();i++)
    {
        cout<<path[i]+1<<' ';
    }
//    for(int i=0;i<N;i++) {
//        for (int j = 0; j < g[i].size(); j++)
//            cout<<i<<": "<<g[i][j]<< ' ';
//        cout<<endl;
//    }

     return 0;
}
