#include <vector>
#include <iostream>

using namespace std;

vector < vector<int> > g;
vector<int> color,path,ans;

void dfs (int v) {
    color[v] = 1;
    path.push_back(v);
    for (auto& x:g[v]) {
        if (color[x] == 0)
            dfs(x);
    }
    if (ans.size()<path.size())
        ans=path;
    path.pop_back();
}
int main(){
    ios_base::sync_with_stdio(false);
    int N;
    cin>>N;
    g=vector<vector<int>>(N);
    for(int i=0;i<N-1;i++){
        int X,Y;
        cin>>X;
        cin>>Y;
        g[X-1].push_back(Y-1);
        g[Y-1].push_back(X-1);
    }
    color.assign(N,0);
    dfs(0);
    color.assign(N,0);
    dfs(ans.back());
    cout<<ans.size();
    return 0;
}

/*
5
1 2
2 3
1 4
4 5


 */
