#include <iostream>
#include <queue>
#include <algorithm>
#include <vector>

using namespace std;

int main ()//LSD
{
    cout<<"Hello World!"<<endl; //7 8 92 -1 0 -3 8 2 -4 -55 -20 -3 40 -1
    int x;
    vector<int>arr;
    while(cin.peek()!='\n'){
        cin>>x;
        arr.push_back(x);
    }
    queue<int> q0;
    queue<int> q1;
    for(int i=0;i<sizeof(int)*8;i++){
        for(int j=0;j<arr.size();j++){
            if (((arr[j]>>i)&1)==1)
                q1.push(arr[j]);
            else
                q0.push(arr[j]);
        }
        arr.clear();
        while(!q0.empty()){
            arr.push_back(q0.front());
            q0.pop();
        }
        while(!q1.empty()){
            arr.push_back(q1.front());
            q1.pop();
        }
    }
    cout<<"Pre rearrange result:\n";
    for(int i=0;i<arr.size();i++)
        cout<<arr[i]<<" ";
    cout<<endl;
    int index=0;
    for(int i=0;i<arr.size()-1;i++)
        if((arr[i]*arr[i+1])<0)
            index = i;
    if (index!=0) {
        reverse(arr.begin(), arr.begin() + index + 1);
        reverse(arr.begin() + index + 1, arr.end());
        reverse(arr.begin(),arr.end());
    }
    cout<<"Rearranged result:\n";
    for(int i=0;i<arr.size();i++)
        cout<<arr[i]<<" ";
    return 0;
}
