#include <vector>
#include <iostream>

using namespace std;


int main(){
    ios_base::sync_with_stdio(false);
    int N;
    cin>>N;
    vector<long long int> sumArray (N+1, 0);
    vector<int> array (N, 0);
    vector<bool> notchecked(N, true);
    sumArray[0]=0;
    for (int i=0; i<N; i++){
        int x;
        cin>>x;
        array[i]=x;
        sumArray[i+1]=sumArray[i]+x;
    }
    cout<<endl;
    for (int i=0; i<N+1; i++){
        cout<<sumArray[i]<<' ';
    }
    cout<<endl;

    long long maxSum=-1000000000;
    int l = 0, r = 0;
    for (int i=0;i<N; i++){
        if (notchecked[i]){
            notchecked[i]=false;
            int index_of_right=-1;
            long long locMaxSum = -1000000000;
            for (int j=i; j<N; j++){
                if (array[j]==array[i]) {
                    notchecked[j] = false;
                    if (locMaxSum<(sumArray[j+1]-sumArray[i])){
                        index_of_right = j;
                        locMaxSum=sumArray[index_of_right+1]-sumArray[i];
                    }
                }
            }
            if (maxSum<locMaxSum){
                maxSum=locMaxSum;
                l = i;
                r = index_of_right;
            }
//            cout<<"----"<<endl;
//            cout<<array[i]<<": "<<endl;
//            cout<<sumArray[index_of_right+1]-sumArray[i]<<endl;
//            cout<<i<<' '<<index_of_right<<endl; //TODO не забыить добавить +1
//            cout<<"----"<<endl;
        }
    }
    cout<<maxSum<<endl<<l+1<<' '<<r+1<<endl;
    return 0;
}
/*
 8
4 57 4 8 5 4 8 57

6
3 5 4 5 3 8


1
-1000000000



 */
