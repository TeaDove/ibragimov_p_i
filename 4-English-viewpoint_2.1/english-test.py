#!/bin/python3

import os
import random
import sys
from pathlib import Path

RED = "\033[91m"
YELLOW = "\033[93;1m"
BLUE = "\033[94m"
CYAN = "\033[36;4m"
MAGENTA = "\033[35;1m"
GREEN = "\033[92;1m"
ENDC = "\033[0m"


os.chdir(Path("/home/teadove/Documents/ibragimov_p_i/Debts/4-English-viewpoint_2.1"))

text_from_file = ""
with open("dict.txt", "r") as f:
    text_from_file = f.read()

words = []
for line in text_from_file.splitlines():
    word = line.split("-")
    words.append(word)
test_len = int(sys.argv[1]) if len(sys.argv) > 1 else 5
test_start = int(sys.argv[2]) if len(sys.argv) > 2 else 0
test_end = int(sys.argv[3]) if len(sys.argv) > 3 else len(words)
is_random = not bool(sys.argv[4]) if len(sys.argv) > 4 else True
sample = random.sample(words[test_start:test_end], k=test_len) if is_random else words[test_start:test_end]

for item in sample:
    print(item[0])
input()
print("-" * 40)
for item in sample:
    print(f"{YELLOW}{item[0].strip()}")
    print(f"{MAGENTA}{item[1].strip()}")
    print(f"{GREEN}{item[2].strip()}")
    print()
