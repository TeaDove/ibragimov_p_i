#!/bin/bash

if [ $USER != "root" ]
then
    echo "Данный скрипт может использовать только root"
else
    echo "Введите имя файлы в данной директории, что хотите скопировать в /usr/temp"
    read fileName
    rm -rf /usr/temp
    mkdir /usr/temp
    ls /usr/temp
    cp $fileName /usr/temp
    ls /usr/temp
fi
