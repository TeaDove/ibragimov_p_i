#!/bin/bash

echo "Введите в разных строках имя файла, что хотети скопировать и путь, куда хотите"
read fileName
read dirName
if [ -z "$(find ./ -name $fileName)" ]||[ -d "{$dirName}"  ]
then
    echo "Такого файла или директорие нет"
else
    cp $fileName $dirName
fi
