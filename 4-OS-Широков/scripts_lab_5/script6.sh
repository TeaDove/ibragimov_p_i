#!/bin/bash

if [ $# -ne 2 ]; then
	echo "Параметров должно быть 2!"
elif ! [ $1 -eq $1 ] 2> /dev/null || ! [ $2 -eq $2 ] 2> /dev/null; then
	echo "Вы ввели не числа"
else
	echo "Cумма: $(($1+$2))"
	echo "Произведение: $(($1*$2))"
	let min=$1
	if [ $1 -gt $2 ]; then
		let min=$2
	fi
	for i in $(seq 1 $min); do
		echo "Hello!"
	done;
fi
