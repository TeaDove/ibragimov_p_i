#!/bin/bash

echo "Введите ваш день рождения";
read user_birth_day; #читаем дату рождения в формате yyyy-mm-dd

flag=true
rm -f script.out.txt #удаляет предыдущий результат работы
for file_name in * #проходимся по всем файлам в папке
do
	if [ $(stat $file_name -c %w | cut -d' ' -f1) = $user_birth_day ]; then #сравниваем birth day файла с введёным пользователя
		if [[ $(stat $file_name -c %F) == 'symbolic link' ]]; then #проверяет является ли тип файлы ссылкой
			echo $file_name >> script.out.txt
			flag=false
		fi
	fi
done
if [ ! -f "script.out.txt" ]; then #проверяем создался ли файл, если нет, то найденных файлов нет
	echo "Таких файлов нет"
fi
