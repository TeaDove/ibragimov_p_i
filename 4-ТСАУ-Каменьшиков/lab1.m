clear;
Um = 1;
w = 5;
a = 0; %не нужная переменная, я знаю...


s = 1i * w;
W_s = 5/(s^2 + s + 3);

A_w = abs(W_s);
f_i = angle(W_s);

t = [0: 0.0001: 10];
u = Um * cos(w * t);

s = tf('s');
W_s = 5/(s^2 + s + 3); %переписываем чтобы впихнуть в lsim

y = lsim(W_s, u, t);

fprintf('A(w) = %.2f\n', roundn(A_w, -2))
fprintf('A(w) * Um = %.2f\n', roundn(A_w*Um, -2))
fprintf('fi(w) = %.2f\n', roundn(f_i, -2)) %округляем до 2х знаком после запятой и выводим до 2х знаков
plot(t, u, t, y), legend("Входной сигнал", "Реакция системы"), title('1 задание')
