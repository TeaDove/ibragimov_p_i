clear;

s = tf('s');
t = [0: 0.0001: 10];
g = t.^2;

W = ((s + 17) / (s  * (s + 18)));
R = (((s + 18) * (2 * s + 1)) / ((s + 17) * s));
w_eg = minreal(1/(1+W*R));
e_g = lsim(w_eg,g,t);

fprintf('e_ginf = %.2f\n',e_g(length(e_g)));
plot( t, g, t, e_g), legend('вход', 'ошибка')
%Исправленно
