clear
%Ну поставьте троечку, ну пожалуйста...

s = tf('s');
q = 1 / 7.2;

n0=q.^3;
m0=3*q^2+9*n0;
m1=3*q;
m2=1;

N = n0;
M = m0*s*s + m1*s + m2;
R = ((s + 17) * M)/ ((s + 6) * N * s * s);
W = (s + 6) / ((s - 9) * (s + 17));
%W_yg = minreal((W * R) / (1 + W * R));
W_yg = minreal(feedback(W * R, 1, -1));

step(W_yg, 10)
info = stepinfo(W_yg, 'SettlingTimeThreshold', 0.05);
legend('h(t)');

settling_time = roundn(info.SettlingTime, -3);
fprintf('%.3f\n', settling_time);
