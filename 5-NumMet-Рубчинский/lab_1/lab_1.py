import math

import numpy as np
import pandas as pd
import seaborn as sns


def func_A(x: float = 0):
    return x**2 + (1 / x)


def func_B(x: float = 0):
    return math.tan(x) + 1


def func(x: float = 0):
    return func_A(x) - func_B(x)


def func_derivative(x: float = 0):
    return 2 * x - (1 / x) ** 2 - (1 / math.cos(x)) ** 2


def draw_func(func_, border_a: float = 0, border_b: float = 10, interval: float = 0.1):
    x_ = np.arange(border_a, border_b, interval)
    array = [[x, func_(x)] for x in x_]
    df = pd.DataFrame(array, columns=["x", "y"])
    print(len(x_), "points")
    sns.lineplot(data=df, x="x", y="y")


# draw_func(func, 0.5, 1.5, 0.0001)
# Метод бисекции
def bisection(func_, x1: float = 0.5, x2: float = 1.5, epsilon: float = 0.0001):
    df = pd.DataFrame(columns=["Xleft", "f(Xleft)", "Xright", "f(Xright)", "delta"])
    f1 = func_(x1)
    f2 = func_(x2)
    del_x = abs(x2 - x1)
    round_base = abs(round(math.log(epsilon, 10)))
    while del_x > epsilon:
        df.loc[len(df)] = list(map(lambda x: round(x, round_base + 1), [x1, f1, x2, f2, del_x]))
        middle = (x2 + x1) / 2
        f_middle = func_(middle)
        if f_middle * f1 < 0:
            x2 = middle
            f2 = f_middle
        else:
            x1 = middle
            f1 = f_middle
        del_x = abs(x2 - x1)
    df.loc[len(df)] = list(map(lambda x: round(x, round_base + 1), [x1, f1, x2, f2, del_x]))
    print(f"Results:\n" f"x = {round(middle, round_base+1)}, iterations = {len(df)}\n")
    with pd.option_context("display.max_rows", None, "display.max_columns", None):
        print(df)
        df.to_excel("/home/teadove/Desktop/lab1.xlsx")


# Метод ложной позиции
def false_position(func_, x1: float = 0.5, x2: float = 1.5, epsilon: float = 0.0001):
    df = pd.DataFrame(columns=["Xleft", "f(Xleft)", "Xright", "f(Xright)", "delta"])
    f1 = func_(x1)
    f2 = func_(x2)
    del_x = abs(x2 - x1)
    round_base = abs(round(math.log(epsilon, 10)))
    while del_x > epsilon:
        df.loc[len(df)] = list(map(lambda x: round(x, round_base + 1), [x1, f1, x2, f2, del_x]))
        middle = x2 * f1 / (f1 - f2) + x1 * f2 / (f2 - f1)
        f_middle = func_(middle)
        if f_middle * f1 > 0:
            del_x = middle - x1
            x1 = middle
            f1 = f_middle
        else:
            del_x = x2 - middle
            x2 = middle
            f2 = f_middle
    df.loc[len(df)] = list(map(lambda x: round(x, round_base + 1), [x1, f1, x2, f2, del_x]))
    print(f"Results:\n" f"x = {round(middle, round_base+1)}\t iterations = {len(df)}\n")
    with pd.option_context("display.max_rows", None, "display.max_columns", None):
        print(df)
        df.to_excel("/home/teadove/Desktop/lab1.xlsx")


# Метод секущии
def secant(func_, x1: float = 0.5, x2: float = 1.5, epsilon: float = 0.0001):
    df = pd.DataFrame(columns=["Xleft", "f(Xleft)", "Xright", "f(Xright)", "delta"])
    f1 = func_(x1)
    f2 = func_(x2)
    del_x = abs(x2 - x1)
    round_base = abs(round(math.log(epsilon, 10)))
    while del_x > epsilon:
        df.loc[len(df)] = list(map(lambda x: round(x, round_base + 1), [x1, f1, x2, f2, del_x]))
        middle = x2 * f1 / (f1 - f2) + x1 * f2 / (f2 - f1)
        f_middle = func_(middle)
        x1 = x2
        f1 = f2
        x2 = middle
        f2 = f_middle
        del_x = abs(x2 - x1)
    df.loc[len(df)] = list(map(lambda x: round(x, round_base + 1), [x1, f1, x2, f2, del_x]))
    print(f"Results:\n" f"x = {round(middle, round_base+1)}, iterations = {len(df)}\n")
    with pd.option_context("display.max_rows", None, "display.max_columns", None):
        print(df)
        df.to_excel("/home/teadove/Desktop/lab1.xlsx")


# Метод ньютона
def newton_method(func_, func_derivative_, x1: float = 0.5, epsilon: float = 0.0001):
    df = pd.DataFrame(columns=["X", "f", "df", "delta"])
    del_x = epsilon * 2
    f1 = func(x1)
    df1 = func_derivative_(x1)
    round_base = abs(round(math.log(epsilon, 10)))
    while del_x > epsilon:
        df.loc[len(df)] = list(map(lambda x: round(x, round_base + 1), [x1, f1, df1, del_x]))
        f1 = func(x1)
        df1 = func_derivative_(x1)
        x2 = x1 - f1 / df1
        del_x = abs(x2 - x1)
        x1 = x2
    df.loc[len(df)] = list(map(lambda x: round(x, round_base + 1), [x1, f1, df1, del_x]))
    print(f"Results:\n" f"x = {round(x1, round_base+1)}, iterations = {len(df)}\n")
    with pd.option_context("display.max_rows", None, "display.max_columns", None):
        print(df)
        df.to_excel("/home/teadove/Desktop/lab1.xlsx")


if __name__ == "__main__":
    bisection(func)
    false_position(func)
    secant(func)
    newton_method(func, func_derivative)
