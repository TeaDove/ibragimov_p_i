#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np

# In[2]:


def false_position(a: list, b: list, c: list):
    xls = 1000000.0
    yls = 1000000.0
    # dv = abs(x-xls)+abs(y-yls)
    dv = 1
    x0, y0 = a
    x1, y1 = b
    x2, y2 = c
    x, y = 0, 0
    while dv > 0.000005:
        zf0 = ef(x0, y0)
        zg0 = ge(x0, y0)
        zf1 = ef(x1, y1)
        zg1 = ge(x1, y1)
        zf2 = ef(x2, y2)
        zg2 = ge(x2, y2)

        Af = (y1 - y0) * (zf2 - zf0) - (y2 - y0) * (zf1 - zf0)
        Bf = (x2 - x0) * (zf1 - zf0) - (x1 - x0) * (zf2 - zf0)
        Cf = (x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0)
        Df = -x0 * Af - y0 * Bf - zf0 * Cf

        Ag = (y1 - y0) * (zg2 - zg0) - (y2 - y0) * (zg1 - zg0)
        Bg = (x2 - x0) * (zg1 - zg0) - (x1 - x0) * (zg2 - zg0)
        Cg = (x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0)
        Dg = -x0 * Ag - y0 * Bg - zg0 * Cg

        dt = Af * Bg - Ag * Bf
        if abs(dt) < 0.000001:
            break
        x = (-Df * Bg + Dg * Bf) / dt
        y = (-Af * Dg + Ag * Df) / dt

        ef(x, y)
        ge(x, y)
        dv = abs(x - xls) + abs(y - yls)

        xls = x
        yls = y

        s0 = stri((x1, y1), (x2, y2), (x, y))
        s1 = stri((x0, y0), (x2, y2), (x, y))
        s2 = stri((x0, y0), (x1, y1), (x, y))
        if s0 >= s1 and s0 >= s2:
            x0 = x1
            y0 = y1
            x1 = x2
            y1 = y2
            x2 = x
            y2 = y
            continue
        if s1 >= s0 and s1 >= s2:
            x1 = x2
            y1 = y2
            x2 = x
            y2 = y
            continue
        if s2 >= s0 and s2 >= s1:
            x2 = x
            y2 = y
            continue
    return round(x, 4), round(y, 4)


# In[3]:


def init_point_choice(m: list, p: list, type_: int) -> list:
    Xm, Ym = m
    Xp, Yp = p
    dl = 0.1
    curmin = 1000000.0
    while curmin > 999999:
        for x in np.arange(Xm, Xp, 0.1):  # (x = Xm; x < Xp; x = x + 0.1)
            for y in np.arange(Ym, Yp, 0.1):  # (y = Ym; y < Yp; y = y + 0.1)
                fc = ef(x, y)
                gc = ge(x, y)
                if type_ == 0 and ((fc <= 0) or (gc <= 0)):
                    continue
                if type_ == 1 and ((fc >= 0) or (gc >= 0)):
                    continue
                if type_ == 2 and ((fc <= 0) or (gc >= 0)):
                    continue
                dv = abs(fc) + abs(gc)
                if dv > curmin:
                    continue
                x0 = x
                y0 = y
                curmin = dv
            if curmin <= 999999.0:
                break
            dl = dl / 2
    return round(x0, 4), round(y0, 4)


# In[4]:


def ef(x: float, y: float) -> float:
    return x**3 - 3 * x * y**2 - x**2 + y**2 + x - 1


def ge(x: float, y: float) -> float:
    return 3 * y * x**2 - y**3 - 2 * x * y + y


# In[5]:


def stri(a: list, b: list, c: list) -> float:
    area = 0.5 * ((a[0] * (b[1] - c[1])) + (b[0] * (c[1] - a[1])) + (c[0] * (a[1] - b[1])))
    return abs(area)


# In[6]:


# for i in range(3):
#   print(init_point_choice((-1, 0), (1, 2), i))


# In[7]:


# false_position((-1.0, 1.1), (0.1, 1.0), (-0.6, 1.9))


# In[8]:


A, B = (-10, 0.1), (999, 999)
false_position(init_point_choice(A, B, 0), init_point_choice(A, B, 1), init_point_choice(A, B, 2))
