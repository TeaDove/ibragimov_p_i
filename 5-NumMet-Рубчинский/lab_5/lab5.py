# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import matplotlib.pyplot as plt
import numpy as np

# %%
# Вариант 6
p = [
    [1.002, 0.166],
    [1.021, 0.505],
    [1.057, 0.857],
    [1.114, 1.232],
    [1.196, 1.647],
    [1.311, 2.118],
    [1.463, 2.668],
    [1.667, 3.334],
    [1.946, 4.173],
    [2.340, 5.288],
]


# %%
def f_0(t: float) -> float:
    if t < -1:
        return 0
    elif -1 <= t < 0:
        return (t + 1) ** 2 / 2
    elif 0 <= t < 1:
        return -((t + 1) ** 2) + 3 * (t + 1) - 3 / 2
    elif 1 <= t < 2:
        return (t + 1) ** 2 / 2 - 3 * (t + 1) + 9 / 2
    elif 2 <= t:
        return 0


# %%
def f_i(i: int, t: float) -> float:
    return f_0(t - i)


# %%
def P(t: float) -> list:
    to_return_x = 0.0
    to_return_y = 0.0
    for idx, point in enumerate(p):
        to_return_x += f_i(idx, t) * point[0]
        to_return_y += f_i(idx, t) * point[1]
    return [round(to_return_x, 5), round(to_return_y, 5)]


# %%
spline = []
q = []


def draw():
    t_list = [el for el in np.arange(0.5, 9.4, 0.05)]
    global spline
    spline = [P(el) for el in t_list]
    q_list = [el for el in np.arange(0, len(p), 1)]
    global q
    q = [P(el) for el in q_list]
    x = [el[0] for el in spline]
    y = [el[1] for el in spline]
    plt.scatter(x, y)

    x = [el[0] for el in p]
    y = [el[1] for el in p]
    plt.scatter(x, y)

    x = [el[0] for el in q]
    y = [el[1] for el in q]
    plt.scatter(x, y)


draw()


# %%
# Trad splines
def get_a(x_0, y_0, x_1, y_1, c_0):
    return (y_0 * x_1 - y_1 * x_0 - c_0 * (x_1 - x_0)) / (x_0 * x_1 * (x_0 - x_1))


print("a_0: ", get_a(p[0][0], p[0][1], p[1][0], p[1][1], 1))
print("a_1: ", get_a(p[1][0], p[1][1], p[2][0], p[2][1], -261.6458005460837))


def get_b(x_0, y_0, x_1, y_1, c_0):
    return (y_0 * x_1**2 - y_1 * x_0**2 - c_0 * (x_1**2 - x_0**2)) / (x_0 * x_1 * (x_1 - x_0))


print("b_0: ", get_b(p[0][0], p[0][1], p[1][0], p[1][1], 1))
print("b_1: ", get_b(p[1][0], p[1][1], p[2][0], p[2][1], -261.6458005460837))


def get_c(x_1, y_1, x_2, y_2, a_0, b_0):
    return (y_1 * (x_2**2 - 2 * x_1 * x_2) + y_2 * x_1**2 - x_1 * x_2 * (x_2 - x_1) * (2 * a_0 * x_1 + b_0)) / (
        (x_2 - x_1) ** 2
    )


print(
    "c_1: ",
    get_c(p[1][0], p[1][1], p[2][0], p[2][1], 18.290343381488047, -19.159259397592344),
)


# %%
def get_trad_spline_func(p: list):
    list_of_coef = []
    c_0 = 1
    a_0 = get_a(p[0][0], p[0][1], p[1][0], p[1][1], 1)
    b_0 = get_b(p[0][0], p[0][1], p[1][0], p[1][1], 1)
    list_of_coef.append([p[0], [a_0, b_0, c_0]])
    idx = 1
    for point in p[1:-1]:
        c = get_c(
            point[0],
            point[1],
            p[idx + 1][0],
            p[idx + 1][1],
            list_of_coef[idx - 1][1][0],
            list_of_coef[idx - 1][1][1],
        )
        a = get_a(point[0], point[1], p[idx + 1][0], p[idx + 1][1], c)
        b = get_b(point[0], point[1], p[idx + 1][0], p[idx + 1][1], c)
        list_of_coef.append([point, [a, b, c]])
        idx += 1

    def func(t: float) -> float:
        idx = 0
        while idx < len(p) - 1 and t > p[idx][0]:
            idx += 1
        if idx != 0:
            idx -= 1

        return list_of_coef[idx][1][0] * t**2 + list_of_coef[idx][1][1] * t + list_of_coef[idx][1][2]

    return func


get_trad_spline_func(p)(1.114)


# %%
trad_spline = []


def trad_draw():
    t_list = [el for el in np.arange(p[0][0] - 0.2, p[-1][0] + 0.2, 0.01)]
    global trad_spline
    trad_func = get_trad_spline_func(p)
    x = [el for el in t_list]
    y = [trad_func(el) for el in t_list]
    trad_spline = [[el[0], el[1]] for el in zip(x, y)]
    plt.scatter(x, y)

    x = [el[0] for el in p]
    y = [el[1] for el in p]
    plt.scatter(x, y)


trad_draw()


# %%
def output_points():
    print("Original points: ")
    str_ = []
    for item in p:
        str_.append(f"({item[0]}, {item[1]})")
    print(",".join(str_))

    str_ = []
    print("\n\nB-spline points: ")
    for item in spline:
        str_.append(f"({item[0]}, {item[1]})")
    print(",".join(str_))

    str_ = []
    print("\n\nTrad-spline points: ")
    for item in trad_spline:
        str_.append(f"({item[0]}, {item[1]})")
    print(",".join(str_))

    str_ = []
    print("\n\nQ points: ")
    for item in q:
        str_.append(f"({item[0]}, {item[1]})")
    print(",".join(str_))


output_points()
