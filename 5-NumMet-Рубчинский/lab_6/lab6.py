# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import math

import numpy as np

real_result = 1.38142933753672


# %%
def func(x: float):
    return (3 * x + 1) / (math.sqrt(5 * x**2 + 1))


# %%
def trapeze_method(f, a: float = -1, b: float = 1, n: int = 6) -> float:
    """Метод трапеции"""
    h = (b - a) / n
    interval = np.arange(a, b + h, h)
    f_interval = list(map(func, interval))
    I = (f_interval[0] + f_interval[-1]) / 2
    I += sum(f_interval[1:-1])
    I *= h
    return I


print(f"Trapeze: {trapeze_method(func)}, discrepancy: {abs(trapeze_method(func) - real_result)}")


# %%
def simpson_method(f, a: float = -1, b: float = 1, n: int = 6) -> float:
    """Метод симпсона"""
    m = n // 2
    h = (b - a) / n
    interval = np.arange(a, b + h, h)
    f_interval = list(map(func, interval))
    sigma_1 = sum(f_interval[1 : 2 * m : 2])
    sigma_2 = sum(f_interval[2 : 2 * m - 1 : 2])
    I = h / 3 * (f_interval[0] + f_interval[-1] + 4 * sigma_1 + 2 * sigma_2)
    return I


print(f"Simpson: {simpson_method(func)}, discrepancy: {abs(simpson_method(func) - real_result)}")


# %%
def gause_method(f, a: float = -1, b: float = 1, n: int = 5) -> float:
    """Метод гауса"""
    summand1 = (a + b) / 2
    summand2 = (b - a) / 2

    def get_t(i: int) -> float:
        if i == 1:
            return -0.906180
        elif i == 2:
            return -0.538469
        elif i == 3:
            return 0
        elif i == 4:
            return 0.538469
        elif i == 5:
            return 0.906180

    def get_a(i: int) -> float:
        if i in [1, 5]:
            return 0.236927
        elif i in [2, 4]:
            return 0.478629
        elif i == 3:
            return 0.568889

    I = 0
    for i in range(1, n + 1):
        I += get_a(i) * f(summand1 + summand2 * get_t(i))
    I *= summand2
    return I


print(f"Gause: {gause_method(func)}, discrepancy: {abs(gause_method(func) - real_result)}")


# %%
# discrepancy
print("Results:")
print(f"Real: {real_result}")
print(f"Trapeze: {trapeze_method(func)}, discrepancy: {abs(trapeze_method(func) - real_result)}")
print(f"Simpson: {simpson_method(func)}, discrepancy: {abs(simpson_method(func) - real_result)}")
print(f"Gause: {gause_method(func)}, discrepancy: {abs(gause_method(func) - real_result)}")
