import numpy as np
from rich import print

# pip install numpy rich


def row_deleter(matrix: np.array, delete_smaller: bool) -> list[int]:
    row_to_delete: set[int] = set()
    for i, row_i in enumerate(matrix):
        for j, row_j in enumerate(matrix):
            if j == i:
                continue

            for k, el_j_k in enumerate(row_i):
                if el_j_k < row_j[k]:
                    break
            else:
                print(f"Row {i} is bigger than {j}")
                print(row_i, ">", row_j, "\n")
                row_to_delete.add(j if delete_smaller else i)
    return list(row_to_delete)


def matrix_solver(matrix: np.array) -> np.array:
    print("Original matrix")
    print(matrix, "\n")
    while True:
        deleted = False
        rows = row_deleter(matrix, delete_smaller=True)
        if rows:
            print(f"Deleting rows: {rows}")
            matrix = np.delete(matrix, rows, axis=0)
            deleted = True
            print("Matrix after row deletion")
            print(matrix, "\n")

        cols = row_deleter(matrix.transpose(), delete_smaller=False)
        if cols:
            print(f"Deleting cols: {cols}")
            matrix = np.delete(matrix, cols, axis=1)
            deleted = True
            print("Matrix after col deletion")
            print(matrix, "\n")

        if not deleted:
            break

    print("Solved matrix")
    print(matrix, "\n")
    return matrix


if __name__ == "__main__":
    result = matrix_solver(
        np.array(
            [
                [7, 2, 5, 3, 7],
                [6, 9, 1, 4, 2],
                [2, 4, 0, 1, 9],
                [4, 6, 0, 3, 1],
            ]
        )
    )
    assert np.array_equal(result, np.array([[2, 5, 3], [9, 1, 4]]))

    result = matrix_solver(
        np.array(
            [
                [4, 7, 9, 4, 6],
                [6, 2, 10, 0, 3],
                [1, 4, 3, 4, 11],
                [3, 2, 9, 2, 4],
            ]
        )
    )
    assert np.array_equal(result, np.array([[4]]))
