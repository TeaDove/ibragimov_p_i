fun binom(n: Int, k: Int): Int {
    println(1)
    if (n == k || k == 0) return 1
    if (k == 1 ) return n
    return binom(n-1, k-1) + binom(n-1, k)
}

fun main(){
    print(binom(6, 2))
}
