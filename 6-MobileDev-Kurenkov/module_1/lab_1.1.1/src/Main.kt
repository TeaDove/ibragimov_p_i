import kotlin.math.max

fun main() {
    val input = readLine().toString().split(' ')
    val k = input[0].toInt()
    val x = input[1].toInt()
    val y = input[2].toInt()

    val max: Int
    val min: Int
    if (x>y) {
        max = x
        min = y
    }else{
        max = y
        min = x
    }
    var tillEnd: Int
    if (k < max){
        tillEnd = maxOf(0, (2 + min - max))
    }else{
        tillEnd = k - max
        val breakaway = k - min
        if (breakaway < 2){
            tillEnd += 2 - breakaway
        }
    }
    println(tillEnd)
}
// 25 24 24
// 2
// 25 22 23
// 2
// 25 25 25
// 2
// 25 24 25
// 1
// 25 10 25
// 0
// 25 10 24
// 1
// 25 30 31
// 1
// 25 29 31
// 0
// 25 10 5
// 15
// 25 30 33
// 0
