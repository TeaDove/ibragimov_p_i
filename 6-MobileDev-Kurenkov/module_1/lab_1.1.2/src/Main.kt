fun main() {
    val input = readLine().toString().split(' ')
    var a = input[0].toDouble()
    var b = input[1].toDouble()
    var n = input[2].toDouble()
    var m = input[3].toDouble()

    if (a < b){
        a = b.apply{b = a}
    }
    if (n < m){
        n = m.apply{m = n}
    }

    if ((n <= a) && (m <= b)){
        println("YES")
    }else{
        println("NO")
    }
}
