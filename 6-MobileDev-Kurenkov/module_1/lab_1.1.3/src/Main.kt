fun main() {
    val input = readLine().toString()

    var counter: HashMap<Char, Int> = hashMapOf('A' to 0, 'T' to 0, 'G' to 0, 'C' to 0)

    for (letter in input) {
        val count = counter[letter] ?: continue
        counter.set(letter, count+1)
    }
    println("${counter['A']} ${counter['T']} ${counter['G']} ${counter['C']}")
}
