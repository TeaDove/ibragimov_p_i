fun countVowels(input: String) : Int{
    val vowels: HashSet<Char> = hashSetOf<Char>('a', 'e', 'i', 'o', 'u')
    var count: Int = 0
    for (letter in input){
        if (vowels.contains(letter)){
            count++
        }
    }
    return count
}
