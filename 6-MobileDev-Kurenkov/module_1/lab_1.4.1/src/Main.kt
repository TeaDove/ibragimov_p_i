//enum class Direction {
//    UP, DOWN, RIGHT, LEFT
//}

class Robot(var x: Int, var y: Int, var direction: Direction){
    fun turnLeft(){
        this.direction = when (direction){
            Direction.UP -> Direction.LEFT
            Direction.DOWN -> Direction.RIGHT
            Direction.RIGHT -> Direction.UP
            Direction.LEFT -> Direction.DOWN
        }
    }
    fun turnRight(){
        this.direction = when (direction){
            Direction.UP -> Direction.RIGHT
            Direction.DOWN -> Direction.LEFT
            Direction.RIGHT -> Direction.DOWN
            Direction.LEFT -> Direction.UP
        }
    }

    fun stepForward(){
        when (direction){
            Direction.UP -> this.y++
            Direction.DOWN -> this.y--
            Direction.RIGHT -> this.x++
            Direction.LEFT -> this.x--
        }
    }

    override fun toString(): String{
        return "x: ${this.x}, y: ${this.y}, dir: ${this.direction}"
    }
}

//fun main(){
//    val r = Robot(0,1,Direction.UP)
//    r.turnRight()
//    r.stepForward()
//    r.stepForward()
//    r.stepForward()
//    r.turnRight()
//    r.stepForward()
//    r.stepForward()
//    r.turnLeft()
//    println(r) // x: 3, y: -1, dir: RIGHT
//}
