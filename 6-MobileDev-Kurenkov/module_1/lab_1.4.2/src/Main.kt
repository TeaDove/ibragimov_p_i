enum class Direction {
    UP, DOWN, RIGHT, LEFT
}

class Robot(var x: Int, var y: Int, var direction: Direction){
    fun turnLeft(){
        this.direction = when (direction){
            Direction.UP -> Direction.LEFT
            Direction.DOWN -> Direction.RIGHT
            Direction.RIGHT -> Direction.UP
            Direction.LEFT -> Direction.DOWN
        }
    }
    fun turnRight(){
        this.direction = when (direction){
            Direction.UP -> Direction.RIGHT
            Direction.DOWN -> Direction.LEFT
            Direction.RIGHT -> Direction.DOWN
            Direction.LEFT -> Direction.UP
        }
    }

    fun stepForward(){
        when (direction){
            Direction.UP -> this.y++
            Direction.DOWN -> this.y--
            Direction.RIGHT -> this.x++
            Direction.LEFT -> this.x--
        }
    }

    override fun toString(): String{
        return "x: ${this.x}, y: ${this.y}, dir: ${this.direction}"
    }
}


fun moveRobot(r: Robot, toX: Int, toY: Int) {
    if (toX > r.x){
        when(r.direction){
            Direction.UP -> {r.turnRight()}
            Direction.DOWN -> {r.turnLeft()}
            Direction.LEFT -> {r.turnRight(); r.turnRight()}
            Direction.RIGHT -> {}
        }
        for(i in 0 until toX - r.x){
            r.stepForward()
        }

    }else if(toX < r.x){
        when(r.direction){
            Direction.UP -> {r.turnLeft()}
            Direction.DOWN -> {r.turnRight()}
            Direction.LEFT -> {}
            Direction.RIGHT -> {r.turnRight(); r.turnRight()}
        }
        for(i in 0 until r.x - toX ){
            r.stepForward()
        }
    }


    if (toY > r.y){
        when(r.direction){
            Direction.UP -> {}
            Direction.DOWN -> {r.turnRight(); r.turnRight()}
            Direction.LEFT -> {r.turnRight()}
            Direction.RIGHT -> {r.turnLeft()}
        }
        for(i in 0 until toY - r.y){
            r.stepForward()
        }
    }else if(toY < r.y){
        when(r.direction){
            Direction.UP -> {r.turnRight(); r.turnRight()}
            Direction.DOWN -> {}
            Direction.LEFT -> {r.turnLeft()}
            Direction.RIGHT -> {r.turnRight()}
        }
        for(i in 0 until  r.y - toY ){
            r.stepForward()
        }
    }

}

fun main(){
    val r = Robot(0,1,Direction.UP)
    r.turnRight()
    r.stepForward()
    r.stepForward()
    r.stepForward()
    r.turnRight()
    r.stepForward()
    r.stepForward()
    r.turnLeft()
    println(r) // x: 3, y: -1, dir: RIGHT

    moveRobot(r, 0, 0)
    println(r)

    moveRobot(r, -10, -10)
    println(r)

    moveRobot(r, -19, 30)
    println(r)
}
