interface Movable {
    fun move(dx: Int, dy: Int)
}

internal class Point(var x:Int, var y:Int) : Movable{
    override fun move(dx: Int, dy: Int) {
        this.x += dx
        this.y += dy
    }

}
internal class CloudOfPoints(val points: ArrayList<Point>) : Movable {
    override fun move(dx: Int, dy: Int) {
        points.forEach{it->
            it.x+=dx
            it.y+=dy
        }
    }
}
