#include <opencv2/opencv.hpp>
#include <ctime>

cv::Mat gamma_cor(const cv::Mat& image, double gamma_);

cv::Mat gamma_cor_direct(const cv::Mat& image, double gamma_);

int main() {
    // Реализация градации через обращению к col изображения
    cv::Mat img(60, 768, CV_8UC1, cv::Scalar(0));
    for(int c = 0; c < img.cols; c++){
        img.col(c).setTo((int) (c/3));
    }
    //cv::imwrite("lab01.png", img);
    unsigned int start, end;

    start = clock();
    cv::Mat img_gamma_direct = gamma_cor_direct(img, 2.2);
    end = clock();
    std::cout << "gamma_cor_direct time " << (float)(end-start) / CLOCKS_PER_SEC * 1000 << "ms." << std::endl ;

    start = clock();
    cv::Mat img_gamma = gamma_cor(img, 2.2);
    end = clock();
    std::cout << "gamma_cor time " << (float)(end-start) / CLOCKS_PER_SEC * 1000 << "ms." << std::endl ;

    //cv::imwrite("lab01_gamma_direct.png", img_gamma_direct);
    //cv::imwrite("lab01_gamma.png", img_gamma);
    cv::Mat result_img;
    cv::vconcat(img, img_gamma, result_img);
    cv::vconcat(result_img, img_gamma_direct, result_img);
    cv::imwrite("lab01.png", result_img);
    return 0;
}

cv::Mat gamma_cor(const cv::Mat& image, double gamma_){
    cv::Mat image_float;
    image.convertTo(image_float, CV_64F);
    image_float /= 255.0;
    cv::pow(image_float, gamma_, image_float);
    image_float *= 255.0;
    image_float.convertTo(image_float, CV_8U);
    return image_float;
}

cv::Mat gamma_cor_direct(const cv::Mat& image, double gamma_){
    cv::Mat image_float;
    image.convertTo(image_float, CV_64F);
    for(int r=0; r<image_float.rows; r++)
        for(int c=0; c<image_float.cols; c++)
            image_float.at<double>(r, c) = (pow(image_float.at<double>(r, c) / 255.0, gamma_) * 255.0);
    image_float.convertTo(image_float, CV_8U);
    return image_float;
}
