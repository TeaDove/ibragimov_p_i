## Работа 1. Исследование гамма-коррекции
автор: Ибрагимов П.И.<br>
группа: БПМ-18-2<br>
дата: 17.02.2021

<!-- https://mysvn.ru/TeaDove/ibragimov_p_i/branches/prj.lab -->

### Задание
1. Сгенерировать серое тестовое изображение I<sub>1</sub> в виде прямоугольника размером 768х60 пикселя с плавным изменение пикселей от черного к белому, одна градация серого занимает 3 пикселя по горизонтали.
2. Применить  к изображению I<sub>1</sub> гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение G<sub>1</sub> при помощи функци pow.
3. Применить  к изображению I<sub>1</sub> гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение G<sub>2</sub> при помощи прямого обращения к пикселям.
4. Показать визуализацию результатов в виде одного изображения (сверху вниз I<sub>1</sub>, G<sub>1</sub>, G<sub>2</sub>).
5. Сделать замер времени обработки изображений в п.2 и п.3, результаты отфиксировать в отчете.

### Результаты

![lab01.png](lab01.png)<br>
Рис. 1. Результаты работы программы (сверху вниз I<sub>1</sub>, G<sub>1</sub>, G<sub>2</sub>)

### Сравнение времени выполнения:
Запустим программу 10 раз
```
╰─λ for i in (seq 1 10); ./lab01; end
gamma_cor_direct time 2.34ms.
gamma_cor time 0.51ms.
gamma_cor_direct time 3.197ms.
gamma_cor time 0.762ms.
gamma_cor_direct time 2.939ms.
gamma_cor time 0.609ms.
gamma_cor_direct time 2.013ms.
gamma_cor time 0.424ms.
gamma_cor_direct time 1.931ms.
gamma_cor time 0.435ms.
gamma_cor_direct time 1.73ms.
gamma_cor time 0.407ms.
gamma_cor_direct time 1.844ms.
gamma_cor time 0.376ms.
gamma_cor_direct time 2.117ms.
gamma_cor time 0.375ms.
gamma_cor_direct time 1.646ms.
gamma_cor time 0.365ms.
gamma_cor_direct time 1.783ms.
gamma_cor time 0.354ms.
```
Проведём анализ полученных данных
- Встроенная функция<br>
Среднее значение: 0.4617<br>
Медианное значение: 0.4155<br>
Дисперсия: 0.1312961453
- Прямое обращение<br>
Среднее значение: 2.154<br>
Медианное значение: 1.972<br>
Дисперсия: 0.5250640173<br>

Как мы видим, отклонения небольшие(0.13 от 0.46 и 0.52 от 2.15), что говорит о том, что по этим данным можно делать выводы. <br>
Легко заметить, что функция \_gamma_cor_(реализация встроенными методами) в среднем в 4.6653 раза быстрее, чем \_gamma_cor_direct(реализация через обращение к каждому пикселю изображения)
### Текст программы

```
#include <opencv2/opencv.hpp>
#include <ctime>

cv::Mat gamma_cor(const cv::Mat& image, double gamma_);

cv::Mat gamma_cor_direct(const cv::Mat& image, double gamma_);

int main() {
    // Реализация градации через обращению к col изображения
    cv::Mat img(60, 768, CV_8UC1, cv::Scalar(0));
    for(int c = 0; c < img.cols; c++){
        img.col(c).setTo((int) (c/3));
    }
    //cv::imwrite("lab01.png", img);
    unsigned int start, end;

    start = clock();
    cv::Mat img_gamma_direct = gamma_cor_direct(img, 2.2);
    end = clock();
    std::cout << "gamma_cor_direct time " << (float)(end-start) / CLOCKS_PER_SEC * 1000 << "ms." << std::endl ;

    start = clock();
    cv::Mat img_gamma = gamma_cor(img, 2.2);
    end = clock();
    std::cout << "gamma_cor time " << (float)(end-start) / CLOCKS_PER_SEC * 1000 << "ms." << std::endl ;

    //cv::imwrite("lab01_gamma_direct.png", img_gamma_direct);
    //cv::imwrite("lab01_gamma.png", img_gamma);
    cv::Mat result_img;
    cv::vconcat(img, img_gamma, result_img);
    cv::vconcat(result_img, img_gamma_direct, result_img);
    cv::imwrite("lab01.png", result_img);
    return 0;
}

cv::Mat gamma_cor(const cv::Mat& image, double gamma_){
    cv::Mat image_float;
    image.convertTo(image_float, CV_64F);
    image_float /= 255.0;
    cv::pow(image_float, gamma_, image_float);
    image_float *= 255.0;
    image_float.convertTo(image_float, CV_8U);
    return image_float;
}

cv::Mat gamma_cor_direct(const cv::Mat& image, double gamma_){
    cv::Mat image_float;
    image.convertTo(image_float, CV_64F);
    for(int r=0; r<image_float.rows; r++)
        for(int c=0; c<image_float.cols; c++)
            image_float.at<double>(r, c) = (pow(image_float.at<double>(r, c) / 255.0, gamma_) * 255.0);
    image_float.convertTo(image_float, CV_8U);
    return image_float;
}
```
### Запуск
```
mkdir build
cd build
cmake ../
make
./lab01
```
