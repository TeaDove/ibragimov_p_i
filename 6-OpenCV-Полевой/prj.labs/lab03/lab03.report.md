## Работа 1. Исследование гамма-коррекции
автор: Ибрагимов П.И.<br>
группа: БПМ-18-2<br>
дата: 24.02.2021

<!-- https://mysvn.ru/TeaDove/ibragimov_p_i/prj.labs/lab02/ -->

### Задание

1. В качестве тестового использовать изображение data/cross_0256x0256.png
2. Сохранить тестовое изображение в формате JPEG с качеством 25%.
3. Используя cv::merge и cv::split сделать "мозаику" с визуализацией каналов для исходного тестового изображения и JPEG-версии тестового изображения
- левый верхний - трехканальное изображение
- левый нижний - монохромная (черно-зеленая) визуализация канала G
- правый верхний - монохромная (черно-красная) визуализация канала R
- правый нижний - монохромная (черно-синяя) визуализация канала B
4. Результы сохранить для вставки в отчет

### Результаты

![](cross_0256x0256_025.jpg)<br>
Рис. 1. Тестовое изображение после сохранения в формате JPEG с качеством 25%

![](cross_0256x0256_png_channels.png)<br>
Рис. 2. Визуализация каналов исходного тестового изображения

![](cross_0256x0256_jpg_channels.png)<br>
Рис. 3. Визуализация каналов JPEG-версии тестового изображения


### Текст программы

```
#include <opencv2/opencv.hpp>
#include <vector>


cv::Mat get_histogram(const cv::Mat & img);

std::vector<cv::Mat> gen_collages(const cv:: Mat & img);

cv::Mat compress_img(const cv::Mat & img, int compress_lvl);

int main() {
    /*
     * закоментированные части кода игнорируйте, я их делал исходя из старого ТЗ
     */
    cv::Mat img = cv::imread("../cross_0256x0256.png");
    std::vector<cv::Mat> collages = gen_collages(img);
    cv::imwrite("../cross_0256x0256_png_channels.png", collages[0]);
//    cv::imwrite("result-origin-hist.jpg", collages[1]);

    cv::Mat img_comp = compress_img(img, 25);
    cv::imwrite("../cross_0256x0256_025.jpg", img_comp);

    std::vector<cv::Mat> collages_comp = gen_collages(img_comp);
    cv::imwrite("../cross_0256x0256_jpg_channels.png", collages_comp[0]);
//    cv::imwrite("result-comp-hist.jpg", collages_comp[1]);
    return 0;
}


std::vector<cv::Mat> gen_collages(const cv:: Mat & img){
    /*
     * Генерация 2х колажей, один обычный, другой гистограммный.
     */
    cv::Mat gray_img;
    cv::cvtColor(img, gray_img, cv::COLOR_BGR2GRAY);
    cv::Mat hist_img = get_histogram(gray_img);
    // Разбиваем на 3 канала, blue, green, red. (именно в таком порядке)
    cv::Mat channels[3];
    cv::split(img, channels);
    cv::Mat result, result_hist, tmp1, tmp2, red, green, blue;
    cv::cvtColor(channels[0], blue, cv::COLOR_GRAY2BGR);
    cv::cvtColor(channels[1], green, cv::COLOR_GRAY2BGR);
    cv::cvtColor(channels[2], red, cv::COLOR_GRAY2BGR);

    cv::Mat hist_blue_img = get_histogram(channels[0]);
    cv::Mat hist_green_img = get_histogram(channels[1]);
    cv::Mat hist_red_img = get_histogram(channels[2]);

    cv::vconcat(hist_img, hist_green_img, result_hist);
    cv::vconcat(hist_red_img, hist_blue_img, tmp1);
    cv::hconcat(result_hist, tmp1, result_hist);

    for (int r = 0; r < red.rows; r++){
        for (int c = 0; c < red.cols; c++) {
            red.at<cv::Vec3b>(r, c)[0] = 0;
            red.at<cv::Vec3b>(r, c)[1] = 0;
        }
    }
    for (int r = 0; r < green.rows; r++){
        for (int c = 0; c < green.cols; c++) {
            green.at<cv::Vec3b>(r, c)[0] = 0;
            green.at<cv::Vec3b>(r, c)[2] = 0;
        }
    }
    for (int r = 0; r < blue.rows; r++){
        for (int c = 0; c < blue.cols; c++) {
            blue.at<cv::Vec3b>(r, c)[1] = 0;
            blue.at<cv::Vec3b>(r, c)[2] = 0;
        }
    }
    cv::vconcat(img, green, result);
    cv::vconcat(red, blue, tmp2);
    cv::hconcat(result, tmp2, result);

    std::vector<cv::Mat> to_return{result, result_hist};
    return to_return;
}


cv::Mat get_histogram(const cv::Mat & img){
    /*
     * Генерация изображения гистограммы яркости по чб изображению
     * @param img cv::Mat чб изображение, по которому строится гистограма
     * @return cv::Mat чб гистограма 512 на 256
     */
    std::vector<int> hist_array(256, 0);
    // Получение гистограммы чб
    int max_pixel = 0;
    for (int row_idx = 0; row_idx < img.rows; row_idx++){
        for (int col_idx = 0; col_idx < img.cols; col_idx++){
            int pixel = (int) img.at<uchar>(row_idx, col_idx);
            hist_array[pixel]++;
            if (hist_array[pixel] > max_pixel)
                max_pixel = hist_array[pixel];
        }
    }
    // Нормализация значений
    for (int & item : hist_array){
        float new_pixel = (float)item / max_pixel * 256;
        item = (int)new_pixel;
    }

    // Рисуем гистограмму
    cv::Mat hist_img(256, 512, CV_8UC1, cv::Scalar(0));
    for(int c = 0; c < hist_img.cols; c+=2){
        for(int r = 0; r < hist_array[c/2]; r++){
            hist_img.at<uchar>(r, c) = 255;
            hist_img.at<uchar>(r, c+1) = 255;
        }
    }
    return hist_img;
}


cv::Mat compress_img(const cv::Mat & img, int compress_lvl){
    std::vector<uchar> buffer;
    std::vector<int> params = {cv::IMWRITE_JPEG_QUALITY, compress_lvl};
    cv::imencode(".jpg", img, buffer, params);
    cv::Mat compressedImage = cv::imdecode(buffer, 1);
    return compressedImage;
}

```

### Запуск
```
mkdir build
cd build
cmake ../
make
./lab02
```
