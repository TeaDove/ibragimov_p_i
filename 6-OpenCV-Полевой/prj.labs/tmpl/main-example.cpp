#include <opencv2/opencv.hpp>

int main() {
    cv::Mat img(60, 768, CV_8UC1);
    cv::Rect2d rc = {0, 0, 3, 60 };
    std::cout << img.rows << " " << img.cols << std::endl;
// TODO сделать через перепись массива точек, а не прямоугольниками
//    for(int i=0; i<=768;i+=3){
//        auto & color = img.at<cv::Vec3b>(0, i);
//        color[0] = 100;
//        std::cout<<i << " " << color << std::endl;
//        img.at<cv::Vec3b>(cv::Point(0, i)) = color;
//    }
    for(int i = 0; i <= 786; i += 3){
//        std::cout << i << " ";
        cv::rectangle(img, rc, {  (double) i / 3 }, -1);
        rc.x = i;
//        std::cout << rc.x << std::endl;
    }
    cv::Mat new_image = cv::Mat::zeros( img.size(), img.type() );
    double alpha = 1.5; /*< Simple contrast control */
    int beta = 0;       /*< Simple brightness control */
    for( int y = 0; y < img.rows; y++ ) {
        for( int x = 0; x < img.cols; x++ ) {
            for( int c = 0; c < img.channels(); c++ ) {
                new_image.at<cv::Vec3b>(y,x)[c] =
                        cv::saturate_cast<uchar>( alpha*img.at<cv::Vec3b>(y,x)[c] + beta );
            }
        }
    }
    cv::imwrite("lab01.png", img);
    cv::imwrite("lab01_1.png", new_image);
    std::cout << std::endl << "done without errors" << std::endl;
}
