import llm
import streamlit as st

title = "Генерация текста с использованием rugpt3small_based_on_gpt2"
st.set_page_config(
    page_title=title,
    page_icon="🧙‍♂️",
    initial_sidebar_state="expanded",
)
st.title(title)

# Ввод текста
txt_input = st.text_area("Введите запрос:", "Как дела?", height=275, key="example_text")

if st.button("Сгенерировать отчёт 📄"):  # при нажатии кнопки на генерацию
    container = st.container()
    placeholder = container.empty()

    response = llm.llm.generate(txt_input)

    placeholder.info(response)
