﻿#include <cmath>

#include <limits>

#include <iostream>

#include <GL/glut.h>


// 4 dimensional float point:   x, y, z, h;
typedef GLfloat Point4Df[4];

const int numPoints = 11; // number of points
const int order = 7; // k (order of polynom)

const float m = 0.9; // 7.9; 

// Point, describing polygon
Point4Df B[numPoints] = {
  {
    0.0,
    0.0,
    0.0,
    1.0
  },
  {
    1.5,
    m,
    0.0,
    1.0
  },
  {
    3.0,
    0.0,
    0.0,
    1.0
  },
  {
    4.5,
    m,
    0.0,
    1.0
  },
  {
    6.0,
    0.0,
    0.0,
    1.0
  },
  {
    7.5,
    m,
    0.0,
    1.0
  },
  {
    9.0,
    0.0,
    0.0,
    1.0
  },
  {
    10.5,
    m,
    0.0,
    1.0
  },
  {
    12.0,
    0.0,
    0.0,
    1.0
  },
  {
    13.5,
    m,
    0.0,
    1.0
  },
  {
    15.0,
    0.0,
    0.0,
    1.0
  }
};


// Colors for basis functions 
GLfloat colorList[11][3] = {
  {
    1.0,
    0.0,
    0.0
  },
  {
    0.0,
    1.0,
    0.0
  },
  {
    0.0,
    0.0,
    1.0
  },
  {
    1.0,
    0.7,
    0.3
  },
  {
    0.0,
    1.0,
    1.0
  },
  {
    1.0,
    0.0,
    1.0
  },
  {
    0.5,
    0.7,
    1.0
  },
  {
    1.0,
    0.5,
    0.3
  },
  {
    0.5,
    0.9,
    0.4
  },
  {
    1.0,
    0.5,
    0.7
  },
  {
    0.0,
    0.0,
    0.0
  },
};

// Amount of key nots should be: n + k
// where n - amount of verticies
// k - order of NURBS
// for period not vector values should be 0, 1, 2, ..., n + k - 1.
// 
GLfloat knot8[] = {
  0.0,
  1.0,
  2.0,
  3.0,
  4.0,
  5.0,
  6.0,
  7.0,
  8.0,
  9.0,
  10.0,
  11.0,
  12.0,
  13.0,
  14.0,
  15.0,
  16.0,
  17.0,
};

// Get element of not version 
GLfloat x(int i) {
    return knot8[i - 1];
}

GLfloat N(int i, int k, GLfloat t) {
    if (k < 1)
        return 0;

    if (k == 1) {
        if ((t >= x(i)) && (t < x(i + 1)))
            return 1;
        else
            return 0;
    }
    else {
        return (t - x(i)) * N(i, k - 1, t) / (x(i + k - 1) - x(i)) +
            (x(i + k) - t) * N(i + 1, k - 1, t) / (x(i + k) - x(i + 1));
    }
}

// Get point by param
void P(GLfloat t, Point4Df& res) {
    memset(res, 0, sizeof(GLfloat) * 4); 

    // Caclulate coordinates for new point
    for (int j = 0; j < 2; j++) 
        for (int i = 1; i <= numPoints; i++) {
            res[j] += B[i - 1][j] * N(i, order, t); 
        }

    res[2] = 0.0; // z
    res[3] = 1.0; // h
}

GLUnurbsObj* nobj;

void init(void) {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    nobj = gluNewNurbsRenderer();
    gluNurbsProperty(nobj, GLU_SAMPLING_TOLERANCE, 1.0);
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);

    glLineWidth(6.0);
    glColor3f(0.0, 0.0, 1.0);
    gluNurbsCurve(nobj, 19, knot8, 4, &B[0][0], order, GL_MAP1_VERTEX_4);
   
    glLineWidth(2.0);
    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_LINE_STRIP); 
    Point4Df res;
    int u = 30; 
    for (int j = 7; j < 11; j++) 
        for (int i = 0; i <= u; i++) {
            GLfloat t = j + (GLfloat)i / (GLfloat)u;
            P(t, res); 
            glVertex4fv(&res[0]); 
        }
    glEnd(); 
    glColor3f(1.0, 0.0, 0.0);
    glPointSize(3.0);
    glBegin(GL_POINTS);
    for (int i = 0; i < numPoints; i++) {
        glVertex4fv(&B[i][0]);
    }
    glEnd();
    glLineWidth(1.0);

    for (int i = 1; i <= 11; i++) {
        glColor3fv(&colorList[i - 1][0]); 
        glBegin(GL_LINE_STRIP);

        GLfloat maxX = 16.5; 
        GLfloat scaleY = 2;
        GLfloat stepY = i * 0; 
        GLfloat offsetY = -(stepY + 1.2);
        u = 480;
        for (int r = 0; r < u; r++) {
            GLfloat t = maxX * (GLfloat)r / (GLfloat)u; 
            glVertex3f(t, scaleY * N(i, order, t) + offsetY, 0.0);
        }

        glEnd();
    }

    glFlush();
}

// Reshape function 
void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei)w, (GLsizei)h); 
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    GLfloat X[] = {
      -1.0,
      17.5
    };
    GLfloat Y[] = {
      -1.5,
      m + 0.5
    };
    // GLfloat Y[] = { -12.5, m + 0.7 };
    GLfloat Z[] = {
      -1.0,
      1.0
    };

    if (w <= h)
        glOrtho(X[0], X[1], Y[0] * (GLfloat)h / (GLfloat)w, Y[1] * (GLfloat)h / (GLfloat)w, Z[0], Z[1]); 
    else
        glOrtho(X[0] * (GLfloat)w / (GLfloat)h, X[1] * (GLfloat)w / (GLfloat)h, Y[0], Y[1], Z[0], Z[1]); 

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
    glutInitWindowSize(480, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("  ");
    init();
    glutDisplayFunc(Display);
    glutReshapeFunc(reshape); 
    glutMainLoop();

    return 0;
}