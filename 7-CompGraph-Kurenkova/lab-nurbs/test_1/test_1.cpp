﻿#include <GL/glut.h>
GLUnurbsObj* nobj;
// массив точек определяющего многоугольника
GLfloat ctlarray[][3] = { { -0.8, -0.4, 0.0 }, { -0.7, -0.2, 0.0 },
                          { -0.6, -0.4, 0.0 }, { -0.5, -0.2, 0.0 },
                          { -0.4, -0.4, 0.0 }, { -0.3, -0.2, 0.0 },
                          { -0.2, -0.4, 0.0 }, { -0.1, -0.2, 0.0 } };
void
init(void)
{
    glClearColor(1.0, 1.0, 1.0, 1.0);
    nobj = gluNewNurbsRenderer();
    gluNurbsProperty(nobj, GLU_SAMPLING_TOLERANCE, 25.0);
}
void
Display()
{
    // массив содержащий значения открытого равномерного узлового вектора
    GLfloat knot[] = { 0.0, 0.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 6.0, 6.0 };

    glClear(GL_COLOR_BUFFER_BIT);
    glLineWidth(2.0);
    glColor3f(1.0, 0.3, 1.0);
    glPointSize(3.0);
    gluNurbsCurve(nobj, 11, knot, 3, &ctlarray[0][0], 3, GL_MAP1_VERTEX_3);
    glBegin(GL_POINTS);
    for (int i = 0; i < 8; i++) {
        glVertex3f(ctlarray[i][0], ctlarray[i][1], ctlarray[i][2]);
    }
    glEnd();
    glFlush();
}
void
main()
{
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
    glutInitWindowSize(480, 960);
    glutInitWindowPosition(20, 20);
    glutCreateWindow("  ");
    init();
    glutDisplayFunc(Display);
    glutMainLoop();
}
