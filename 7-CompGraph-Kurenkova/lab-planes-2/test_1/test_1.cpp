﻿// Поверхность Безье

#include <cmath>
#include <iostream>
#include <GL/glut.h>

// Тип, определяющий точку 3D
typedef GLfloat Point3Df[3];

// Поверхность Безье в матричном виде (см. стр. 439, формула 6-59)
// Q(u, w) = [U][N][B][M]T[W]
// для бикубической поверхности Безье [N] = [M]T - для бикубической поверхности Безье эти матрицы имеют констрантные значения, т.е. Q(u, w) = [U][N][B][N][W]
// [U] - вектор, определяемый параметрами u
// [W] - вектор, определяемый параметрами w
// [B] - матрица, определяющая вершины задающей полигонной сетки

// Вершины задающей полигонной сетки
Point3Df B[4][4] = {
			{{ 0.0,  0.0, 1.0}, {-0.5,-1.0, 1.0},  {0.5, -1.0, 1.0}, {1.0,  -1.0, 1.0}},
			{{-1.0, -0.5, 1.0}, {-0.5,-0.5, 1.0},  {0.5, -0.5, 1.0}, {1.0,  -0.5, 1.0}},
			{{-1.0,  0.5, 1.0}, {-0.5, 0.5, 1.0},  {0.5,  0.5, 1.0}, {1.0,   0.5, 1.0}},
			{{-1.0,  1.0, 1.0}, {-0.5, 1.0, 1.0},  {0.5,  1.0, 1.0}, {0.0,   0.0, 1.0}}
};

// Тип, определяющий матрицу 4x4
typedef GLfloat Matrix_4x4_f[4][4];

// Тип, определяющий вектор
typedef GLfloat Vector_1x4_f[4];

// [N] = [M]T
Matrix_4x4_f N = { {-1.0,  3.0, -3.0, 1.0},
								   { 3.0, -6.0,  3.0, 0.0},
									 {-3.0,  3.0,  0.0, 0.0},
									 { 1.0,  0.0,  0.0, 0.0}
};

// Получить вектор для параметра u или v
void T(GLfloat t, Vector_1x4_f& res)
{
	res[0] = std::powf(t, 3);
	res[1] = std::powf(t, 2);
	res[2] = t;
	res[3] = 1;
}

// Получить вершины задающей полигонной сетки для одного измерения
void maxrixOneDim(int dim, Matrix_4x4_f& res)
{
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++) {
			res[i][j] = B[i][j][dim];
		}
}

// Умножение матриц 4x4
void matrixMult(Matrix_4x4_f& a, Matrix_4x4_f& b, Matrix_4x4_f& res)
{
	memset(res, 0, sizeof(GLfloat) * 4 * 4); // Обнуляем буфер

	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			for (int r = 0; r < 4; r++) {
				res[i][j] += a[i][r] * b[r][j];
			}
}

// Умножение вектора 1x4 на матрицу 4x4
void matrixMult(Vector_1x4_f& a, Matrix_4x4_f& b, Vector_1x4_f& res)
{
	memset(res, 0, sizeof(GLfloat) * 4); // Обнуляем буфер

	for (int j = 0; j < 4; j++)
		for (int r = 0; r < 4; r++) {
			res[j] += a[r] * b[r][j];
		}
}

// Умножение вектора 1x4 на вектор 4x1
void matrixMult(Vector_1x4_f& a, Vector_1x4_f& b, GLfloat& res)
{
	res = 0;

	for (int r = 0; r < 4; r++) {
		res += a[r] * b[r];
	}
}

// Точка на поверхности Безье
void Q(GLfloat u, GLfloat w, Point3Df& res)
{
	Matrix_4x4_f t1;
	Matrix_4x4_f t2;
	Matrix_4x4_f B_dim;
	Vector_1x4_f U;
	Vector_1x4_f W;
	Vector_1x4_f t3;

	T(u, U); // U - вектор для параметра u
	T(w, W); // W - вектор для параметра w

	// Проходим по каждому измерению: Координата X, Y, Z: (i=0)=>X; (i=1)=>Y; (i=2)=>Z; 
	for (int i = 0; i < 3; i++) {
		maxrixOneDim(i, B_dim);     // Получить вершины задающей полигонной сетки для i-го измерения  
		matrixMult(N, B_dim, t1);   // [t1] = [N][B] 
		matrixMult(t1, N, t2);      // [t2] = [t1][N], т.к. [N] = [M]T
		matrixMult(U, t2, t3);      // [t3] = [U][t2]
		matrixMult(t3, W, res[i]);  // res[i] = [t3][W];  res[i] - значение i-й координаты
	}

	// Выполнив все итерации цикла получаем одну точку поверхности Q(u, w) = [U][N][B][N][W]
}

// Функция формирования содержимого программного окна
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    // Очистка заданных буферов
	glColor3f(1.0, 1.0, 1.0);                              // Установка цвета объектов

	Point3Df res;

	// Количество вычисляемых точек (jN + 1)*(iN + 1)
	int jN = 32;
	int iN = 32;
	for (int j = 0; j <= jN; j++)
	{
		glBegin(GL_LINE_STRIP);                                              // Начало формирования заданного примитива
		for (int i = 0; i <= iN; i++) {
			Q((GLfloat)i / (GLfloat)iN, (GLfloat)j / (GLfloat)jN, res);        // Точка на поверхности Безье
			glVertex3fv(&res[0]);                                              // Добавляем точку к примитиву  
		}
		glEnd();                                                             // Конец формирования заданного примитива

		glBegin(GL_LINE_STRIP);                                              // Начало формирования заданного примитива
		for (int i = 0; i <= iN; i++) {
			Q((GLfloat)j / (GLfloat)jN, (GLfloat)i / (GLfloat)iN, res);        // Точка на поверхности Безье
			glVertex3fv(&res[0]);                                              // Добавляем точку к примитиву  
		}
		glEnd();                                                             // Конец формирования заданного примитива
	}

	glFlush(); // Команда OpenGl для обработки всех ранее помещенных в очередь команд
}

// Функция, вызывающаяся по изменению размеров программного окна
void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h); // Устанавливает размеры прямоугольника, в который осуществляется графический вывод под размер всего программного окна
	glMatrixMode(GL_PROJECTION);              // Устанавливает текущей матрицей проекционную матрицу. Последующие настройки будут применяться к ней
	glLoadIdentity();                         // Инициализация текущей матрицы

	GLfloat L = 1;
	if (w <= h)
		glOrtho(-L, L, -L * (GLfloat)h / (GLfloat)w, L * (GLfloat)h / (GLfloat)w, -L, L); // умножает текущую матрицу на ортографическую матрицу: ограничиваем заданный объем, где будет выполняться рисование
	else
		glOrtho(-L * (GLfloat)w / (GLfloat)h, L * (GLfloat)w / (GLfloat)h, -L, L, -L, L); // ... другие параметры в зависимости от ширины/высоты окна

	glMatrixMode(GL_MODELVIEW);              // Устанавливает текущей видовую матрицу
	glLoadIdentity();                        // Инициализация текущей матрицы 
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);                        // Инициализация библиотеки glut

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);  // Задаем параметры окна
	glutInitWindowSize(500, 500);                 // Задаем размеры окна
	glutInitWindowPosition(100, 100);             // Задаем позицию окна
	glutCreateWindow("Поверхность Безье. Точки, соединенные линиями"); // Создание окна
	glClearColor(0.0, 0.0, 0.0, 0.0);             // Установка цвета фона (цвет заполнения буфера цвета при вызове glClear)
	glutDisplayFunc(display);                     // Регистрация функции обратного вызова, формирующей содержимое программного окна
	glutReshapeFunc(reshape);                     // Регистрация функции обратного вызова, вызывающейся по изменению размеров программного окна
	glutMainLoop();                               // Основной цикл программы 
	return 0;
}
