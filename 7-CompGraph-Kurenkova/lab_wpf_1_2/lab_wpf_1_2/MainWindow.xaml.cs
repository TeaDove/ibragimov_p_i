﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lab_wpf_1_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded()
        {
            PointCollection mypointcoll = new PointCollection(6)
            {
                new Point(60,250),
                new Point(110,250),
                new Point(160,150),
                new Point(210,50),
                new Point(260,50),
                new Point(310,150),
            };
            PolyBezierSegment mypolyBezierSegment = new PolyBezierSegment();
            mypolyBezierSegment.Points = mypointcoll;
            PathFigure pathFigure = new PathFigure();
            pathFigure.StartPoint = new Point(10, 150);
            pathFigure.Segments.Add(mypolyBezierSegment);
            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(pathFigure);
            Path path = new Path();
            path.Stroke = Brushes.Green;
            path.Data = pathGeometry;
            this.canvas1.Children.Add(path);
        }
    }
}
