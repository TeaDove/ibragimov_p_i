```mermaid
classDiagram
direction BT

class channel {
   int64 tg_id
   string tg_title
   string tg_username
   string tg_about
   bool is_leaf
   datetime uploaded_at
   int64 participant_count
   array(int64) recommendations_ids
}

class channel_edge {
   int64 tg_id_in
   int64 tg_id_out
   int64 order
}
channel_edge --|> channel : tg_id_in->tg_id
channel_edge --|> channel : tg_id_out->tg_id


class message {
   datetime created_at
   int64 tg_chat_id
   int64 tg_id
   int64 tg_user_id
   string text
   int64 reply_to_msg_id
   int64 reply_to_user_id
   uint64 words_count
   uint64 toxic_words_count
}
message --|> channel : tg_chat_id->tg_id


```
