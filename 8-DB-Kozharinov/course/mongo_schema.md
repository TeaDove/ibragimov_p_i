```mermaid
classDiagram
direction BT

class chats {
   objectid _id
   timestamp created_at
   timestamp updated_at

   int64 tg_id
   string title
}

class messages {
    objectid _id
    timestamp created_at
    timestamp updated_at

    string text
    int64 tg_chat_id
    int32 tg_id
    int64 tg_user_id
}
messages --|> users : tg_user_id->tg_id
messages --|> chats : tg_chat_id->tg_id

class images {
    objectid _id
    timestamp created_at
    timestamp updated_at

    []byte content
}

class tg_images {
    objectid _id
    timestamp created_at
    timestamp updated_at

    tg.InputPhoto(object) tg_input_photo
    objectid message_id
    objectid image_id
}

tg_images --|> images : image_id->_id
tg_images --|> messages : message_id->_id

class kandinsky_images {
    objectid _id
    timestamp created_at
    timestamp updated_at

    kandinsky_supplier.RequestGenerationInput(object) input
    objectid image_id
    objectid tg_image_id
}

kandinsky_images --|> images : image_id->_id
kandinsky_images --|> tg_images : tg_image->_id

class ping_messages {
    objectid _id
    timestamp created_at
    timestamp updated_at

    timestamp delete_at
    objectid message_id
}
ping_messages --|> messages : objectid->_id

class restart_messages{
    objectid _id
    timestamp created_at
    timestamp updated_at

    objectid message_id
}
restart_messages --|> messages : objectid->_id

class users {
    objectid _id
    timestamp created_at
    timestamp updated_at

    int64 tg_id
    string tg_name
    string tg_username
    boolean is_bot
}

class members {
    objectid _id
    timestamp created_at
    timestamp updated_at

    string status
    int64 tg_chat_id
    int64 tg_user_id
}
members --|> users : tg_user_id->tg_id
members --|> chats : tg_chat_id->tg_id
```
