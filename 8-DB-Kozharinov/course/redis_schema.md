```mermaid
classDiagram
direction BT

class ban {
   key ban::[tg_user_id]
}

class chat-settings {
   hkey chat-settings::[tg_chat_id]

   bool     enabled
   string   locale
   int8     tz
   []byte   features
}

class reg-rule {
   hkey reg-rule::[tg_chat_id]

   string regexp -> string rule
}
```
